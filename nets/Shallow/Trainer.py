# -*- coding: utf-8 -*-
"""
Created on Sun Jun  3 15:08:40 2018

@author: felip
""" 
import tensorflow as tf
import numpy as np

class Trainer:
    
    def __init__(self, sess, model, data, config, logger):
        
        self.epochs = config['epochs']
        
        self.model = model
        self.data = data
        self.sess = sess
        self.logger = logger
        self.x_test = self.data.x_test
        self.y_test = self.data.y_test
        self.x_train = self.data.x_train
        self.y_train = self.data.y_train
        
        self.init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(self.init)
        
    def train(self):
        self.max_validation_f1 = 0
        self.training_loss = []
        for i in range(self.epochs):
            self.train_epoch()
            self.sess.run(self.model.increment_cur_epoch_tensor)

        
    def train_epoch(self):
        
        losses = []
        precs = []
        accs = []
        recalls = []
        f1s = []
        loop = tqdm(range(self.data.num_batch))
        for _ in loop:
            loss, acc, prec,recall,f1= self.train_step()
            losses.append(loss)
            accs.append(acc)
            precs.append(prec)
            recalls.append(recall)
            f1s.append(f1)
            
        self.training_loss.append(np.mean(losses))
        cur_it = self.model.global_step_tensor.eval(self.sess)

        print("Epoch {0}, training loss is {1}, training accuracy is {2}, precision is {3}, recall is {4}, f1 score is {5}".format(self.model.cur_epoch_tensor.eval(self.sess),np.mean(losses),np.mean(accs),np.mean(precs),np.mean(recalls),np.mean(f1s)))
        summaries_dict = {
                'loss':np.mean(losses),
                'accuracy': np.mean(accs),
                'precision': np.mean(precs),
                'recall': np.mean(recalls),
                'f1':np.mean(f1s)
        }
        
        self.logger.summarize(cur_it,summaries_dict = summaries_dict)
        
        
        #testing
        feed_dict = {self.model.x: self.x_test, self.model.y: self.y_test, self.model.is_training : False}                
        loss, labels,preds = self.sess.run([self.model.loss,self.model.labels,self.model.prediction], feed_dict = feed_dict)
        prec, recall, f1, acc = self.precision_recall_f1_accuracy(np.array(labels),np.array(preds))
        
        print("Testing loss is {0}, testing accuracy is {1}, precision is {2}, recall is {3}, f1 score is {4}".format(loss,acc,prec,recall,f1),end = '\n')
        summaries_dict = {
                'loss':loss,
                'accuracy': acc,
                'precision': prec,
                'recall': recall,
                'f1':f1}
                
        self.logger.summarize(cur_it,summaries_dict = summaries_dict,summarizer="test")
        #saving
        if f1 > self.max_validation_f1:
            self.max_validation_f1 = f1
            self.model.save(self.sess)
            
    def train_step(self):
        x,y = next(self.data.next_batch())
        feed_dict = {self.model.x: x, self.model.y: y,  self.model.is_training : True}                
        _ , loss, labels, preds = self.sess.run([self.model.train_step,self.model.loss, self.model.labels,self.model.prediction], feed_dict = feed_dict)
        prec, recall, f1, acc = self.precision_recall_f1_accuracy(np.array(labels),np.array(preds))
        return loss, acc, prec, recall, f1
    
    def precision_recall_f1(self,labels,predictions):
        uniques = np.unique(labels)
        #number of classes
        n = uniques.shape[0]
        precision = []
        recall = []
        f1 = []
        for k in range(n):
            cur_label = uniques[k]
            tp = ((labels == cur_label) & (predictions == cur_label)).sum()
            fp = ((labels != cur_label) & (predictions == cur_label)).sum()
            fn = ((labels == cur_label) & (predictions != cur_label)).sum()
            if tp + fp != 0:
                recall.append(tp/(tp + fn))
                if tp == 0:
                    precision.append(0)
                    f1.append(0)
                else:
                    precision.append(tp/(tp + fp))
                    f1.append(2*precision[-1]*recall[-1]/(precision[-1] + recall[-1]))
        precision = np.array(precision)
        recall = np.array(recall)
        f1 = np.array(f1)
        return np.mean(precision), np.mean(recall), np.mean(f1)

    def plot_training_loss(self):
        plt.plot(self.training_loss)
        
    def train_from_checkpoint(self,checkpoint,config = None):
        if config:
            self.config = config