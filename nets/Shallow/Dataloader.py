
class Data():
    
    def __init__(self, x_train, y_train, x_test, y_test, config):
        '''
        Inputs:
            x_train : np.array of shape (nb of train instances, features)
            y_train : np.array of shape (nb of train instances,) containing all labels
            x_test : np.array of shape (nb of test instances, features)
            y_test : np.array of shape (nb of test instances,) containing all labels
            config : dict with all hyperparameters and configurations
                    * batch_size
                    * epochs
                    * learning_rate
                    * checkpoint_dir
                    * max_to_keep
            
        '''
        self.label_encoder =  preprocessing.LabelBinarizer()
        self.label_encoder.fit(np.append(y_train,y_test,axis = 0))
        self.y_train = self.label_encoder.transform(y_train)
        self.y_test = self.label_encoder.transform(y_test)
        self.x_train = x_train
        self.x_test = x_test
        #shuffling train data
        self.x_train,self.y_train = self.shuffle_features_and_labels(self.x_train,self.y_train)
        
        self.unique_labels = np.unique(y_train)
        self.num_of_classes = self.unique_labels.shape[0]
        self.batch_size = config['batch_size']
        assert(self.batch_size > self.num_of_classes)
        self.batch_size -= self.batch_size%self.num_of_classes
        self.num_batch = self.x_train.shape[0]//self.batch_size
        
        self.x_train,self.y_train = self.shuffle_features_and_labels(self.x_train,self.y_train)
        self.labels_and_features_dic =  self.create_dic_with_labels_and_features(self.x_train,self.y_train)
        self.balanced_x_train, self.balanced_y_train = self.make_balanced_batches()
        #self.balanced_x_train, self.balanced_y_train = self.shuffle_features_and_labels(self.balanced_x_train,self.balanced_y_train)
        
        
    '''
    shuffles two arrays, keeping indexes correspondances
    '''    
    def shuffle_features_and_labels(self,features,labels):
            #shuffling data
            randomize = np.arange(features.shape[0])
            np.random.shuffle(randomize)
            features = features[randomize]
            labels = labels[randomize]
            return features, labels
    
    
    def create_dic_with_labels_and_features(self,features,labels):
        d = {}
        labels = self.label_encoder.inverse_transform(labels)
        for i in range(labels.shape[0]):
            label = labels[i]
            if label in d.keys():
                d[label] = np.append(np.expand_dims(features[i],axis = 0),d[label], axis = 0)
            else:
                d[label] = np.expand_dims(features[i],axis = 0)
        return d
        
    def make_balanced_batches(self):
        
       
        num_per_batch = int(self.batch_size/self.num_of_classes)
        balanced_x_train = []
        balanced_y_train = []
        for key in list(self.labels_and_features_dic.keys()):
            indexes = np.array([i%len(self.labels_and_features_dic[key]) for i in range(self.num_batch*num_per_batch)])
            selected = self.labels_and_features_dic[key][indexes]
            y_selected = np.array(self.label_encoder.transform([key for _ in range(selected.shape[0])]))
            for obj in range(selected.shape[0]):
                balanced_x_train.append(selected[obj])
                balanced_y_train.append(y_selected[obj])
        return np.array(balanced_x_train), np.array(balanced_y_train)     
    
    def next_batch(self):
        idx = np.random.choice(self.balanced_x_train.shape[0],self.batch_size)
        yield self.balanced_x_train[idx],self.balanced_y_train[idx]
        
        
    def array_name_at_level(self,labels,level):
        for i in range(labels.shape[0]):
            labels[i] = self.name_at_level(labels[i],level)
        return labels
    
    def name_at_level(self,string,level):
        '''
        receives a string with the entire label for a certain object
        and cuts the right half
        :param string: the label as a string
        :param level: desired level
        '''
        index = -len(string.split('/'))+level+3
        index = min(len(string.split('/')) + index, len(string.split('/'))+1)
        out = '/'.join(string.split('/')[:index])
        if out[-1] != '/':
            out += '/'
        return out   