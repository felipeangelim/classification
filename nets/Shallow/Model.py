# -*- coding: utf-8 -*-
"""
Created on Sun Jun  3 13:52:23 2018

@author: felip
"""
import tensorflow as tf

class ClassifierNet():
    def __init__(self, feature_dim, classes_num, config):
        self.init_global_step()
        self.feature_dim = feature_dim
        self.classes_num = classes_num
        
        # init the global step
        self.init_global_step()
        # init the epoch counter
        self.init_cur_epoch()
        self.config = config
        self.learning_rate = config['learning_rate']
        #build graph
        self.build_network()
        #init saver
        self.init_saver()
        #create checkpoint directory if it doesn't exist
        self.checkpoint_dir = config['checkpoint_dir']
        if not os.path.exists(self.checkpoint_dir):
            os.makedirs(self.checkpoint_dir)
        
    def build_network(self):
        
        self.is_training = tf.placeholder(tf.bool)
        self.x = tf.placeholder(tf.float32, [None, self.feature_dim])
        self.y = tf.placeholder(tf.float32, [None, self.classes_num])
        self.class_weights  = tf.placeholder(tf.float32, [self.classes_num])
        
        self.dense = tf.layers.dense(inputs = self.x, units = 128, activation = tf.nn.relu,use_bias = True,name = 'dense1')
        self.dropout = tf.layers.dropout(inputs = self.dense, rate = 0.7, training = self.is_training)
        self.logits = tf.layers.dense(inputs = self.dropout, units = self.classes_num, name = 'logits')

        with tf.name_scope("loss"):
            self.loss =  tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels = self.y, logits = self.logits)) 
            self.train_step = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss, global_step =  self.global_step_tensor)
            
            self.prediction = tf.argmax(self.logits,1)
            self.labels = tf.argmax(self.y,1)
            #accuracy
            correct_prediction = tf.equal(self.prediction,self.labels)
            self.accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))
            
            
    def init_saver(self):
        tf.global_variables_initializer()
        self.saver = tf.train.Saver(max_to_keep=self.config['max_to_keep'])
    
    def init_global_step(self):
        # DON'T forget to add the global step tensor to the tensorflow trainer
        with tf.variable_scope('global_step'):
            self.global_step_tensor = tf.Variable(0, trainable=False, name='global_step')
    
    def init_cur_epoch(self):
        with tf.variable_scope('cur_epoch'):
            self.cur_epoch_tensor = tf.Variable(0, trainable=False, name='cur_epoch')
            self.increment_cur_epoch_tensor = tf.assign(self.cur_epoch_tensor, self.cur_epoch_tensor + 1)
            
    
    def save(self, sess):
        print("Saving model...")
        self.saver.save(sess, self.checkpoint_dir, self.global_step_tensor)
        print("Model saved")
    
    def load(self, sess):
        latest_checkpoint = tf.train.latest_checkpoint(self.checkpoint_dir)
        if latest_checkpoint:
            print("Loading model checkpoint {} ...\n".format(latest_checkpoint))
            tf.reset_default_graph()
            self.saver.restore(sess, latest_checkpoint)
#            weights = tf.get_default_graph().get_tensor_by_name(os.path.split(self.dense_4.name)[0] + '/kernel:0')
#            sess.run(tf.variables_initializer([weights]))
            print("Model loaded")
            
    def load_path(self, sess, path):
        print("Loading model checkpoint {} ...\n".format(path))
        self.saver.restore(sess, path)
        print("Model loaded")
    
    def dense(x, num_in, num_out, name, relu=True):
        """Create a fully connected layer."""
        with tf.variable_scope(name) as scope:

            # Create tf variables for the weights and biases
            weights = tf.get_variable('weights', shape=[num_in, num_out],
                                      trainable=True)
            biases = tf.get_variable('biases', [num_out], trainable=True)

            # Matrix multiply weights and inputs and add bias
            act = tf.nn.xw_plus_b(x, weights, biases, name=scope.name)

        if relu:
            # Apply ReLu non linearity
            relu = tf.nn.relu(act)
            return relu
        else:
            return act  