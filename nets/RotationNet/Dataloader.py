# -*- coding: utf-8 -*-
"""
Created on Sun Jun  3 14:52:54 2018

@author: felip
"""
import sys
sys.path.insert(0, '../')
from preprocessing.prepare_txt_data import prepare_txt_data
import numpy as np
import math
from collections import Counter
import os
from tensorflow.python.framework.ops import convert_to_tensor
from tensorflow.python.framework import dtypes
import tensorflow as tf



class Data():
    
    def __init__(self,
                 train_txt,
                 test_txt,
                 config,
                 sess,
                 seed = 42,
                 verbose = False,
                 shuffle_buffer_size = 1000,
                 prefetch_buffer_size = 100):
        tf.set_random_seed(seed)
        self.seed = seed
        '''
        Inputs:
            x_train : np.array of shape (nb of train instances, features)
            y_train : np.array of shape (nb of train instances,) containing all labels
            x_test : np.array of shape (nb of test instances, features)
            y_test : np.array of shape (nb of test instances,) containing all labels
            config : dict with all hyperparameters and configurations
            
        '''
        self.config = config
        self.test_dir = config['data_dir'] + 'test/'
        self.train_dir = config['data_dir'] + 'train/'
        self.VIEWS_NUM = config['views_num'] 
        self.BATCH_SIZE = config['batch_size']
        self.OBJS_PER_CLASS = config['objs_per_class']
        self.OBJS_PER_BATCH = int(self.BATCH_SIZE/self.VIEWS_NUM)
        self.MODEL = config["model"]
        self.LABEL_INDEX = config['label_index']
        self.ROTATIONNET_CASE = config['rotationnet_case']
        self.VCANDS = np.load(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                           'vcand_case'+ str(self.ROTATIONNET_CASE) + ".npy"))
        self.CANDS_NUM = self.VCANDS.shape[0]
        self.data_dir = config['data_dir']
        self.train_txt = train_txt
        self.test_txt = test_txt
        
        #array of labels
        self.unique_labels = []
        self.unique_labels = np.array([name for name in os.listdir(self.train_dir) if os.path.isdir(os.path.join(self.train_dir, name))])
        
        #set number of classes
        self.CLASSES_NUM = len(self.unique_labels)
        print(self.unique_labels)
        #create a label encoder
        self.label_encoder =  LabelEncoder()
        self.label_encoder.fit(self.unique_labels)
        self.label_encoder.add('i_view')

        print(' [RotationNet] Number of classes: ', self.CLASSES_NUM)
        print('[RotationNet] Number of views: ', self.VIEWS_NUM)
        print('Data dir: ', config['data_dir'])
        

        reuse = os.path.isfile(train_txt) and os.path.isfile(test_txt) 
        if not reuse:
            prepare_txt_data(all_dir = self.data_dir,
                             path_out_train = train_txt,
                             path_out_test = test_txt,
                             n_views = self.VIEWS_NUM,
                             objs_per_class = self.OBJS_PER_CLASS,
                             shuffle_patch_size = self.VIEWS_NUM,
                             label_index = self.LABEL_INDEX,
                             verbose = verbose)
        
        #read txt_files
        self.x_train, self.gather_inds_score_train, self.gather_inds_loss_train, self.y_train,self.target_train = self._read_txt_file(train_txt)
        self.x_test, self.gather_inds_score_test, self.gather_inds_loss_test, self.y_test,self.target_test = self._read_txt_file(test_txt)
        
        self.test_size = len(self.y_test)
        self.train_size = len(self.y_train)
        self.num_batch = (len(self.x_train)//self.BATCH_SIZE)
        if (len(self.x_train)%self.BATCH_SIZE) != 0:
            self.num_batch += 1
        
        #convert lists to TF tensor
        self.x_train = convert_to_tensor(self.x_train, dtype = dtypes.string)
        self.gather_inds_score_train = convert_to_tensor(self.gather_inds_score_train, dtype = dtypes.int32)
        self.gather_inds_loss_train = convert_to_tensor(self.gather_inds_loss_train, dtype = dtypes.int32)
        self.y_train = convert_to_tensor(self.y_train, dtype = dtypes.int32)
        self.target_train = convert_to_tensor(self.target_train, dtype = dtypes.int32)
        
        self.x_test = convert_to_tensor(self.x_test, dtype = dtypes.string)
        self.gather_inds_score_test = convert_to_tensor(self.gather_inds_score_test, dtype = dtypes.int32)
        self.gather_inds_loss_test = convert_to_tensor(self.gather_inds_loss_test, dtype = dtypes.int32)
        self.y_test = convert_to_tensor(self.y_test, dtype = dtypes.int32)
        self.target_test = convert_to_tensor(self.target_test, dtype = dtypes.int32)
        
        #create TF dataset
        self.x_train = tf.data.Dataset.from_tensor_slices(self.x_train)
        self.y_train = tf.data.Dataset.from_tensor_slices(self.y_train)
        self.gather_inds_score_train = tf.data.Dataset.from_tensor_slices(self.gather_inds_score_train)
        self.gather_inds_loss_train = tf.data.Dataset.from_tensor_slices(self.gather_inds_loss_train)
        self.target_train = tf.data.Dataset.from_tensor_slices(self.target_train)
        
        #zip
        self.train_dataset = tf.data.Dataset.zip((self.x_train,
                                                  self.gather_inds_score_train,
                                                  self.gather_inds_loss_train,
                                                  self.y_train,
                                                  self.target_train))
        
        self.x_test = tf.data.Dataset.from_tensor_slices(self.x_test)
        self.y_test = tf.data.Dataset.from_tensor_slices(self.y_test)
        self.gather_inds_score_test = tf.data.Dataset.from_tensor_slices(self.gather_inds_score_test)
        self.gather_inds_loss_test = tf.data.Dataset.from_tensor_slices(self.gather_inds_loss_test)
        self.target_test = tf.data.Dataset.from_tensor_slices(self.target_test)
        self.test_dataset = tf.data.Dataset.zip((self.x_test,
                                                  self.gather_inds_score_test,
                                                  self.gather_inds_loss_test,
                                                  self.y_test,
                                                  self.target_test))
        
        print('Mapping...')
        self.train_dataset = self.train_dataset.map(self._parse_function_train,num_parallel_calls = 6)
        self.test_dataset = self.test_dataset.map(self._parse_function_test,num_parallel_calls = 6)
        
        #TODO shuffle
        self.train_dataset = self.train_dataset.repeat().batch(self.BATCH_SIZE).prefetch(self.BATCH_SIZE*2)
        self.test_dataset = self.test_dataset.repeat().batch(self.BATCH_SIZE).prefetch(self.BATCH_SIZE*2)
        

        #We create a feedable iterator
        self.handle = tf.placeholder(tf.string, shape=[])
        iterator = tf.data.Iterator.from_string_handle(self.handle,
                                                       self.train_dataset.output_types,
                                                       self.train_dataset.output_shapes)
        self.next_element = iterator.get_next()
        
        #training iterator
        self.training_iterator = tf.data.Iterator.from_structure(self.train_dataset.output_types,
                                               self.train_dataset.output_shapes)  
        
        #testing iterator
        self.testing_iterator = tf.data.Iterator.from_structure(self.train_dataset.output_types,
                                               self.train_dataset.output_shapes)  
        
        self.training_handle = sess.run(self.training_iterator.string_handle())
        self.testing_handle = sess.run(self.testing_iterator.string_handle())
        
        #init operations 
        self.training_init_op = self.training_iterator.make_initializer(self.train_dataset)
        self.testing_init_op = self.testing_iterator.make_initializer(self.test_dataset)
    
    def _parse_function_train(self,filename,gather_score, gather_loss,classes,target):
        """
        This function parses each element of the training set
        """
        
        #load and preprocess the image
        img_string = tf.read_file(filename)
        #decode png image
        img_decoded = tf.image.decode_png(img_string, channels=3)
        #do data augmentation
        img_resized = self._data_augmentation(img_decoded)
        #convert to 0 - 1 float
        img = tf.image.convert_image_dtype(img_resized,tf.float32)/255
        #keep values in the 0 - 1 range
        img = tf.clip_by_value(img,0,1)
        img = tf.subtract(img, 0.5)
        img = tf.multiply(img, 2.0)
        
        return img,gather_score, gather_loss,classes, target
    
    def _parse_function_test(self,filename,gather_score, gather_loss,classes,target):
        """
        This function parses each element of the testing set
        """
        one_hot = classes
        
        # load and preprocess the image
        img_string = tf.read_file(filename)
        #decode png image
        img_decoded = tf.image.decode_png(img_string, channels=3)
        #resize the image
        img_resized = tf.image.resize_images(img_decoded, [224, 224])
        #convert to 0 - 1 float
        img = tf.image.convert_image_dtype(img_resized,tf.float32)/255
        img = tf.subtract(img, 0.5)
        img = tf.multiply(img, 2.0)
        
        return img,gather_score, gather_loss,classes,target

    def _data_augmentation(self, img):
        """ 
        Define the data augmentation procedure
        Inputs:
            - img: RGB image before resizing
        Outputs:
            - img_resized: resized and augmented image 
        
        """
        
        #img_resized = tf.image.random_hue(img,0.5,self.seed)
        """
        def to_grayscale(img):
            img_resized = tf.image.rgb_to_grayscale(img)
            img_resized = tf.image.grayscale_to_rgb(img_resized)
            return img_resized
        
        def constrast_brightness(img):
            img =tf.cond(tf.less(tf.random_uniform(shape=[], minval=0, maxval=1, dtype=tf.float32), 0.5),
                    true_fn = lambda: tf.image.random_contrast(tf.image.random_brightness(img, 0.25, self.seed), 0.5, 1.5),
                    false_fn = lambda: tf.image.random_brightness(tf.image.random_contrast(img, 0.5, 1.5), 0.25, self.seed))
            return img
        
        def do_nothing(img):
            return img
        
        img = tf.cond(tf.less(tf.random_uniform(shape=[], minval=0, maxval=1, dtype=tf.float32), 0.5),
                true_fn = lambda: constrast_brightness(img),
                false_fn = lambda: do_nothing(img))
        
        img = tf.cond(tf.less(tf.random_uniform(shape=[], minval=0, maxval=1, dtype=tf.float32), 0.5),
                true_fn = lambda: to_grayscale(img),
                false_fn = lambda: do_nothing(img))
        """
        #random crop image
        img_resized = tf.image.resize_images(img, [224,224])
        img_resized = tf.random_crop(img_resized,[224,224,3],seed = self.seed)
        return img_resized    
    
    def _read_txt_file(self,txt_file):
        """
        Read the content of the text file and store it into lists.
        Inputs:
            txt_file: string, the to the .txt file
        Outputs:
            img_paths: array, paths to the .png images
            gather_inds_score:
            gather_inds_loss:
            classes: array, the ground-truth class for each image
            target: array, the target output matrix for each 
        """

        # the paths to the images
        img_paths = [] 
        # the variable 'target' contains the target for each output, which is 
        # one vector of with VIEWS_NUM dimensions, e.g. 
        # [i_view,i_view,class1,i_view], to be one-hot encoded
        target = [] 
        # the class of each image
        classes = [] 
        # index of elements to be gathered to calculate the score for each view candidate
        gather_inds_score = []
        # index of view candidates to be gathered to calculate the loss
        gather_inds_loss = []

        print("Reading text file...")
        with open(txt_file, 'r') as f:
            lines = f.readlines()
            for line in lines:
                img_paths.append(line[:-1]) # we avoid the '\n'
                #parsing class encoding
                class_name = line.split('/')[self.LABEL_INDEX]
                class_number = self.label_encoder.transform([class_name])[0]
                classes.append(class_number)

        ind = 0
        # iterate for each object in the dataset
        for c in classes[0:len(classes):self.VIEWS_NUM]:
            # the index of the object in the current batch
            obj_index_cur_batch = ind%self.OBJS_PER_BATCH
            # for each object, we create a list with all possible matches of views and the column index 'c' of the
            # target class for calculating the score
            temp = [[obj_index_cur_batch,k%self.VIEWS_NUM,
                     self.VCANDS[k//self.VIEWS_NUM,k%self.VIEWS_NUM],c] for k in range(self.VIEWS_NUM*self.CANDS_NUM)]
            gather_inds_score.extend(temp)

            # similarly, we create a multidimensional array from all possible view candidates
            temp = [[obj_index_cur_batch,k%self.VIEWS_NUM,
                     self.VCANDS[k//self.VIEWS_NUM,k%self.VIEWS_NUM]] for k in range(self.VIEWS_NUM*self.CANDS_NUM)]
            gather_inds_loss.extend(temp)
            ind += 1
           
        self.classes = classes
        try:
            assert(len(classes)%self.VIEWS_NUM == 0)
        except AssertionError:
            print('Some images are missing.')
            raise AssertionError
        
        gather_inds_score = np.array(gather_inds_score)
        gather_inds_loss = np.array(gather_inds_loss)
        gather_inds_score=  gather_inds_score.reshape([np.prod(gather_inds_score.shape)//(self.CANDS_NUM*4), self.CANDS_NUM,4])
        gather_inds_loss=  gather_inds_loss.reshape([np.prod(gather_inds_loss.shape)//(self.CANDS_NUM*3), self.CANDS_NUM,3])
        self.gather_inds_score = np.array(gather_inds_score) #* for debugging
        
        target = []
        for c in classes:
                # we create, for each view, a target output vector
                matrix_bef_onehot = []
                view = 0
                for cand in range(self.CANDS_NUM):
                    t = [self.CLASSES_NUM for i in range(self.VIEWS_NUM)]
                    t[self.VCANDS[cand,view]] = c
                    matrix_bef_onehot.append(t)
                view += 1
                target.append(matrix_bef_onehot)
        # target of shape [n_objs, cands_num, views_num]
        
        return img_paths,gather_inds_score,gather_inds_loss,classes,target
            
class LabelEncoder:
    def __init__(self):
        pass
    
    def fit(self,y):
        y = np.array(y)
        self.uniques = np.unique(y)
        self.size = self.uniques.shape[0]
        self.i_encoding = {}
        self.encoding = {}
        for i in range(self.size):
            self.i_encoding[i] = self.uniques[i]
            self.encoding[self.uniques[i]] =  i
    def transform(self,array):
        array = np.array(array)
        new_array = []
        for i in range(array.shape[0]):
            new_array.append(self.encoding[array[i]])
        return np.array(new_array)
    
    def inverse_transform(self,array):
        array = np.array(array)
        new_array = []
        for i in range(array.shape[0]):
            new_array.append(self.i_encoding[array[i]])
        return np.array(new_array)
    
    def add(self,label):
        self.i_encoding[self.size] = label
        self.encoding[label] = self.size
        self.size += 1
    
    