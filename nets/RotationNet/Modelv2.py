# -*- coding: utf-8 -*-
"""
Created on Sun Jun  3 13:52:23 2018

@author: felip
"""
import sys
sys.path.insert(0, '../')
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tqdm import tqdm
import os
from tensorflow.contrib import slim

class RotationNet():
    def __init__(self, data,config):
        self.config = config
        self.data = data
        
        self.init_global_step()
        
        #parameters
        self.CLASSES_NUM = config['classes_num']
        self.VIEWS_NUM = config['views_num']
        self.OPTIMIZER = config['optimizer'].lower()
        self.MOMENTUM = config['momentum']
        self.LEARNING_RATE = config['learning_rate']
        self.KEEP_PROB = config['keep_prob']
        self.WEIGHTS_PATH = config["weights_path"]
        self.MODEL = config["model"]
        self.BATCH_SIZE = config["batch_size"]
        self.VARS_TO_TRAIN = config["vars_to_train"]
        self.DECAY_FACTOR = config['decay_factor']
        self.WEIGHT_DECAY = config["weight_decay"]
        self.NUM_EPOCHS_PER_DECAY = config['num_epochs_per_decay']
        self.OBJS_PER_BATCH = int(self.BATCH_SIZE/self.VIEWS_NUM)
        self.CANDS_NUM = self.data.CANDS_NUM # number of candidates == number of views

        
        
        

        # init the epoch counter
        self.init_cur_epoch()
        #build the network
        self.build_network()
        #initialize saver
        self.init_saver()
        
        self.checkpoint_dir = config['checkpoint_dir']
        if not os.path.exists(self.checkpoint_dir):
            os.makedirs(self.checkpoint_dir)
            
    def build_network(self):
        '''  Build the model graph '''
        assert(self.data.BATCH_SIZE%self.VIEWS_NUM == 0)
        
        self.is_training = tf.placeholder(tf.bool)
        
        #get data from Input Pipeline
        self.x,self.classes, self.target = self.data.next_element
        
        ########### MODEL DEFINITION ###########
        if self.MODEL == "inception_resnet_v2":
            import base_models.inception_resnet_v2 as inception_resnet_v2
            with slim.arg_scope(inception_resnet_v2.inception_resnet_v2_arg_scope()):
                self.logits, self.end_points  = inception_resnet_v2.inception_resnet_v2(self.x,
                                                             num_classes = (self.CLASSES_NUM+1)*self.VIEWS_NUM,
                                                             dropout_keep_prob = self.KEEP_PROB,
                                                             create_aux_logits=True,
                                                             is_training= False)
        elif self.MODEL == "inception_v1":
            import base_models.inception_v1 as inception_v1
            with slim.arg_scope(inception_v1.inception_v1_arg_scope()):
                self.logits, self.end_points  = inception_v1.inception_v1(self.x,
                                                             num_classes = (self.CLASSES_NUM+1)*self.VIEWS_NUM,
                                                             dropout_keep_prob = self.KEEP_PROB,
                                                             is_training = False)
        elif self.MODEL == "mobilenet_v1_1.0_224":
            import base_models.mobilenet_v1 as mobilenet_v1
            with slim.arg_scope(mobilenet_v1.mobilenet_v1_arg_scope()):
                self.logits, self.end_points  = mobilenet_v1.mobilenet_v1(self.x,
                                                             num_classes = (self.CLASSES_NUM+1)*self.VIEWS_NUM,
                                                             dropout_keep_prob = self.KEEP_PROB,
                                                             is_training = False)                          
        else:
            raise ValueError('Model not available.')
        ########### END ###########

        #List of tensorflow variables to apply the optimizer    
        var_list = [v for v in tf.trainable_variables()][-self.VARS_TO_TRAIN:]
            
        print('[RotationNet]', len(var_list), 'trainable variables')
        print('[RotationNet] Training from', var_list[0], 'up to', var_list[-1])
        
        # Matrix of shape(batch_size, number of views, number of classes +1)
        # See equation (2) in the original article for more information
        self.output_matrix_simple = tf.reshape(self.logits,[-1,self.VIEWS_NUM, self.CLASSES_NUM + 1])
        
    
        
        # indices to gather
        self.vcands = self.data.VCANDS.flatten()
        self.repeated_obj_numbers = np.reshape(np.tile(np.expand_dims(np.arange(self.OBJS_PER_BATCH), axis = 1),[1,self.VIEWS_NUM*self.CANDS_NUM]), (-1,1)) # 5x20x60
        self.repeated_view_numbers = np.reshape(np.tile(np.arange(self.VIEWS_NUM), [self.OBJS_PER_BATCH*self.CANDS_NUM, 1]),(-1,1)) # 5x20x60
        self.repeated_view_cands = np.reshape(np.tile(self.vcands, self.OBJS_PER_BATCH), (-1,1))
        self.indices_gather_loss = tf.constant(np.concatenate((self.repeated_obj_numbers,self.repeated_view_numbers,self.repeated_view_cands), axis = 1), dtype = tf.int32)

        self.repeated_obj_class = tf.reshape(tf.tile(tf.reshape(self.classes, [self.OBJS_PER_BATCH, self.VIEWS_NUM]), [1,self.CANDS_NUM]), [-1,1])
        self.indices_gather_score = tf.concat(axis = 1,values = [self.indices_gather_loss,self.repeated_obj_class])
        

        # Here we reshape our output matrix. Our new shape is:
        # (number of different objects, number of views per object in the input, (output_matrix.shape))
        self.output_matrix = tf.reshape(self.output_matrix_simple,[self.OBJS_PER_BATCH,self.VIEWS_NUM, self.VIEWS_NUM, self.CLASSES_NUM + 1])
        #Log of the elements in the matrix
        self.log_p = tf.log(tf.nn.softmax(self.output_matrix,axis = -1))
        #we remove the i_view column and calculate log(p(i,j)) - log(p(i, CLASSES_NUM + 1))
        self.scores_ind = self.log_p[:,:,:,:-1] - tf.tile(tf.expand_dims(self.log_p[:,:,:,-1],-1),[1,1,1,self.CLASSES_NUM])
        # we gather the elements which corresponds with each view candidate possibility
        self.gather_temp = tf.gather_nd(self.scores_ind, self.indices_gather_score)
        self.scores = tf.reshape(self.gather_temp,[self.OBJS_PER_BATCH,
                                                   self.CANDS_NUM,
                                                   self.VIEWS_NUM])
        # Calculate the scores for each view candidate (Equation 3 in the original article)                                                                                    
        self.scores = tf.reduce_sum(self.scores,axis = -1) #[[]]
        # Get the best candidate
        self.best_score_per_obj = tf.cast(tf.argmax(self.scores,axis = -1),tf.int32) #[]
        self.best_score_per_obj = tf.expand_dims(self.best_score_per_obj, -1) #[[]]
        inc = tf.expand_dims(tf.range(tf.shape(self.scores)[0]),-1) #[[],[],[]]
        self.best_cand_per_obj = tf.concat((inc,self.best_score_per_obj),1)# [[0,i0], [1,i1]]...
        
        #self.indices_gather_loss = tf.gather_nd(self.indices_gather_score,self.inds)
        #tf reshape gather loss [objs per batch, candidates, i1,i2]
        self.indices_gather_loss = tf.reshape(self.indices_gather_loss, [self.OBJS_PER_BATCH,self.CANDS_NUM,self.VIEWS_NUM,3])
        self.indices_gather_loss = tf.gather_nd(self.indices_gather_loss,self.best_cand_per_obj)
        
        self.target_reshaped = tf.reshape(self.target, [self.OBJS_PER_BATCH, self.VIEWS_NUM, self.CANDS_NUM,self.VIEWS_NUM])
        self.target_vectors = tf.gather_nd(self.target_reshaped, self.indices_gather_loss)
                
        
        # Train op
        with tf.name_scope("train"):
            #loss function
            with tf.name_scope("loss"):
               
                self.classes_onehot = tf.one_hot(self.target_vectors, depth = self.CLASSES_NUM+1)
                self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels = self.classes_onehot, logits = self.output_matrix_simple))
                
                #l2 loss (improves the performance)
                for var in var_list:
                    self.loss += tf.nn.l2_loss(var)*self.WEIGHT_DECAY

                self.predictions = self.select_best(self.logits)
                self.classes = tf.reshape(self.classes,[-1,self.VIEWS_NUM])[:,0]
            
            # Get gradients of all trainable variables
            gradients = tf.gradients(self.loss, var_list)
            gradients = list(zip(gradients, var_list))
            
            # learning rate
            decay_steps = int(self.data.train_size / self.BATCH_SIZE * self.NUM_EPOCHS_PER_DECAY)
            self.learning_rate = tf.train.exponential_decay(self.LEARNING_RATE,
                                                            self.global_step_tensor,
                                                            decay_steps,
                                                            self.DECAY_FACTOR,
                                                            staircase=True)

            # Create optimizer and apply gradient descent to the trainable variables
            # We apply different optimization methods for each case
            if self.OPTIMIZER == 'momentum':
                self.learning_rate = tf.constant(self.LEARNING_RATE)
                optimizer_top = tf.train.MomentumOptimizer(self.LEARNING_RATE,self.MOMENTUM)
                optimizer_bottom = tf.train.MomentumOptimizer(self.LEARNING_RATE/100,self.MOMENTUM)
            elif self.OPTIMIZER == "rmsprop":
                optimizer_top = tf.train.RMSPropOptimizer(self.learning_rate,decay = 0.9,momentum = self.MOMENTUM)
                optimizer_bottom = tf.train.RMSPropOptimizer(self.learning_rate/100,decay = 0.9,momentum = self.MOMENTUM)
            elif self.OPTIMIZER == "adam":
                optimizer_top = tf.train.AdamOptimizer(self.learning_rate)
                optimizer_bottom = tf.train.AdamOptimizer(self.learning_rate/100)

            #self.train_step = optimizer.apply_gradients(grads_and_vars=gradients, global_step =  self.global_step_tensor)
            
            # setting different training ops for each part of the network
            # we set a smaller lr for the layers in the middle
            var_list_bottom = [v for v in tf.trainable_variables()][:-self.VARS_TO_TRAIN]
            gradients = tf.gradients(self.loss, var_list_bottom + var_list)
            grads_bottom = gradients[:-self.VARS_TO_TRAIN]
            grads_top = gradients[-self.VARS_TO_TRAIN:]
            train_op_bottom = optimizer_bottom.apply_gradients(zip(grads_bottom, var_list_bottom), global_step = self.global_step_tensor)
            train_op_top = optimizer_top.apply_gradients(zip(grads_top, var_list))
            self.train_step = tf.group(train_op_bottom, train_op_top)
  
    def select_best(self,fc8):
        #shape batch_size x P matrix
        fc8 =  tf.reshape(fc8,[-1, self.VIEWS_NUM, self.CLASSES_NUM + 1])
        #apply softmax on the rows of the P matrix
        fc8 = tf.log(tf.nn.softmax(fc8,axis = -1))
        self.softmax_fc8 = fc8
        #divide all probs by the probability of incorrect view (shape (batch_size,view_num,classes_num))
        i_view_probability_to_sub = tf.tile(tf.expand_dims(fc8[...,-1],-1),[1,1,self.CLASSES_NUM])
        fc8 = fc8[...,:-1]
        score = fc8 - i_view_probability_to_sub
        self.score1 = score
        # score per image per object
        score =  tf.reshape(score,[self.OBJS_PER_BATCH,self.VIEWS_NUM, self.VIEWS_NUM, self.CLASSES_NUM])
        self.score = tf.reduce_sum(tf.reduce_sum(score, axis = -2),axis = -2)
        best_classes = tf.argmax(self.score,axis = -1)
        return tf.cast(best_classes,dtype = tf.int32)
        
    def select_best_v2(self, fc):
        """
        Uses max criterion

        """
        #shape batch_size x P matrix
        fc =  tf.reshape(fc,[-1, self.VIEWS_NUM, self.CLASSES_NUM + 1])
        #apply softmax on the rows of the P matrix
        fc = tf.log(tf.nn.softmax(fc,axis = -1))
        self.softmax_fc8 = fc8
        #divide all probs by the probability of incorrect view (shape (batch_size,view_num,classes_num))
        i_view_probability_to_sub = tf.tile(tf.expand_dims(fc8[...,-1],-1),[1,1,self.CLASSES_NUM])
        fc = fc[...,:-1]
        score = fc8 - i_view_probability_to_sub
        self.score1 = score
        # score per image per object
        score =  tf.reshape(score,[self.OBJS_PER_BATCH,self.VIEWS_NUM * self.VIEWS_NUM, self.CLASSES_NUM])
        best_classes = tf.argmax(tf.reduce_max(self.score,axis = -2), axis = -1)
        return tf.cast(best_classes,dtype = tf.int32)   

    def select_best_v3(self, fc):
        """
        Uses something similar to score
        
        """
        #shape batch_size x P matrix
        fc =  tf.reshape(fc,[-1, self.VIEWS_NUM, self.CLASSES_NUM + 1])
        #apply softmax on the rows of the P matrix
        fc = tf.log(tf.nn.softmax(fc,axis = -1))
        self.softmax_fc8 = fc8
        #divide all probs by the probability of incorrect view (shape (batch_size,view_num,classes_num))
        i_view_probability_to_sub = tf.tile(tf.expand_dims(fc8[...,-1],-1),[1,1,self.CLASSES_NUM])
        fc = fc[...,:-1]
        score = fc8 - i_view_probability_to_sub
        self.score1 = score
        # score per image per object
        score =  tf.reshape(score,[self.OBJS_PER_BATCH,self.VIEWS_NUM, self.VIEWS_NUM, self.CLASSES_NUM])
        best_classes = tf.argmax(tf.sum(tf.reduce_max(self.score,axis = -2), axis = -1))
        return tf.cast(best_classes,dtype = tf.int32)  
        
    def load_initial_weights(self, sess):
        """Load weights from file into network.

        As the weights from http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/
        come as a dict of lists (e.g. weights['conv1'] is a list) and not as
        dict of dicts (e.g. weights['conv1'] is a dict with keys 'weights' &
        'biases') we need a special load function
        """
        # Load the weights into memory
        pretrained_checkpoint = self.WEIGHTS_PATH
        if self.MODEL ==    "inception_resnet_v2":
            checkpoint_exclude_scopes=["InceptionResnetV2/Logits", "InceptionResnetV2/AuxLogits"]
        if self.MODEL == "inception_v1":
            checkpoint_exclude_scopes=["InceptionV1/Logits", "InceptionV1/AuxLogits"]
        if self.MODEL == "mobilenet_v1_1.0_224":
            checkpoint_exclude_scopes=["MobilenetV1/Logits", "MobilenetV1/AuxLogits", "fully_connected"]
        if self.MODEL == "inception_v1_2":
            checkpoint_exclude_scopes=["InceptionV1/Logits", "InceptionV1/AuxLogits","fully_connected_1","fully_connected"]
    
        exclusions = [scope.strip() for scope in checkpoint_exclude_scopes]
        
        variables_to_restore = []
        for var in slim.get_model_variables():
            for exclusion in exclusions:
                if var.op.name.startswith(exclusion):
                    break
            else:
                variables_to_restore.append(var)
                
        print("Restoring variables up to", var)
        init_fn = slim.assign_from_checkpoint_fn(pretrained_checkpoint,variables_to_restore)
        init_fn(sess)
                    
    def initialize_uninitialized(self,sess):
        global_vars          = tf.global_variables()
        is_not_initialized   = sess.run([tf.is_variable_initialized(var) for var in global_vars])
        not_initialized_vars = [v for (v, f) in zip(global_vars, is_not_initialized) if not f]
    
        print([str(i.name) for i in not_initialized_vars]) # only for testing
        if len(not_initialized_vars):
            sess.run(tf.variables_initializer(not_initialized_vars))     
            
    def init_saver(self):
        tf.global_variables_initializer()
        self.saver = tf.train.Saver(max_to_keep=self.config['max_to_keep'])
    
    def init_global_step(self):
        # DON'T forget to add the global step tensor to the tensorflow trainer
        with tf.variable_scope('global_step'):
            self.global_step_tensor = tf.Variable(0, trainable=False, name='global_step')
    
    def init_cur_epoch(self):
        with tf.variable_scope('cur_epoch'):
            self.cur_epoch_tensor = tf.Variable(0, trainable=False, name='cur_epoch')
            self.increment_cur_epoch_tensor = tf.assign(self.cur_epoch_tensor, self.cur_epoch_tensor + 1)
            
    
    def save(self, sess):
        print("Saving model...")
        self.saver.save(sess, self.checkpoint_dir, self.global_step_tensor)
        print("Model saved")
    
    def load(self, sess):
        latest_checkpoint = tf.train.latest_checkpoint(self.checkpoint_dir)
        if latest_checkpoint:
            print("Loading model checkpoint {} ...\n".format(latest_checkpoint))
            self.saver.restore(sess, latest_checkpoint)
            print("Model loaded from {}".format(latest_checkpoint))
            
    def load_path(self, sess, path):
        print("Loading model checkpoint {} ...\n".format(path))
        self.saver.restore(sess, path)
        print("Model loaded")
        
    def inference(self,sess):
        sess.run(self.data.testing_init_op)
        predictions = []
        probs = []
        classes = []
        for _ in tqdm(range(int(self.data.test_size/self.data.BATCH_SIZE))):
            feed_dict = {self.is_training : False}     
            predicted_classes,correct_classes, score = sess.run([self.predictions, self.classes, self.score], feed_dict = feed_dict)
            probs.append(score)
            predictions.extend(predicted_classes)
            classes.extend(correct_classes)
        return np.array(probs), np.array(predictions), np.array(classes)
        
    def load_trainable(self,sess):
        tf.global_variables_initializer()
        self.saver = tf.train.Saver(var_list = tf.trainable_variables(), max_to_keep=self.config['max_to_keep'])
        self.load(sess)