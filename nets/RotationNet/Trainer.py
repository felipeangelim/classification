# -*- coding: utf-8 -*-
"""
Created on Sun Jun  3 15:08:40 2018

@author: felip
"""

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tqdm import tqdm_notebook as tqdm
from math import sqrt

        
class Trainer:
    
    def __init__(self, sess, model, data, config, logger):        
        self.epochs = config['epochs']        
        self.tests_per_epoch = config['tests_per_epoch']
        self.model = model
        self.data = data
        self.sess = sess
        self.logger = logger
        self.x_test = self.data.x_test
        self.y_test = self.data.y_test
        
    def train(self):
        self.training_loss = []
        self.max_f1 = 0
        for i in range(self.epochs):
            self.train_epoch()
            self.sess.run(self.model.increment_cur_epoch_tensor)

        
    def train_epoch(self):
        self.sess.run(self.data.training_init_op)
        self.sess.run(self.data.testing_init_op)
        for __ in range(self.tests_per_epoch):
            # loop over batches
            losses = []
            predictions = []
            classes = []
            loop = tqdm(range(self.data.num_batch//self.tests_per_epoch))
            lrs = []
            for _ in loop:
                loss, predicted_classes,correct_classes,lr = self.train_step()
                losses.append(loss)
                predictions.extend(predicted_classes)
                classes.extend(correct_classes)
                lrs.append(lr)
            #calculate mean of all losses in the epoch
            self.training_loss.append(np.mean(losses))
            # get current iteration
            cur_it = self.model.global_step_tensor.eval(self.sess)
            # print some useful information
            
            precision, recall, f1, acc = self.precision_recall_f1_accuracy(np.array(classes),np.array(predictions))
            print('Learning rate is',np.mean(lrs))
            print("Epoch {0}, training loss is {1}, recall is {2}, precision is {3}, f1 is {4}, accuracy is {5}".format(self.model.cur_epoch_tensor.eval(self.sess),self.training_loss[-1], recall,precision,f1,acc))
            summaries_dict = {
                    'loss':np.mean(losses),
                    'acc': acc,
                    'precision': precision,
                    'recall': recall,
                    'f1': f1
            }
            #pass information to the logger
            self.logger.summarize(cur_it,summaries_dict = summaries_dict)
            
            
            # **** testing ****
            self.sess.run(self.data.testing_init_op)
            
            testing_loss = []
            predictions = []
            classes = []
            for _ in tqdm(range(int(self.data.test_size/self.data.BATCH_SIZE))):
                loss,  predicted_classes,correct_classes = self.test_step()
                testing_loss.append(loss)
                predictions.extend(predicted_classes)
                classes.extend(correct_classes)
                
            
            precision, recall, f1, acc = self.precision_recall_f1_accuracy(np.array(classes),np.array(predictions))
            print("Testing loss is {0}, recall is {1}, precision is {2}, f1 is {3}, accuracy is {4}...".format(np.mean(testing_loss),recall,precision,f1,acc),end = '\n')
            summaries_dict = {
                            'loss':np.mean(testing_loss),
                            'acc': acc,
                            'precision': precision,
                            'recall': recall,
                            'f1': f1
                            }
            
            self.logger.summarize(cur_it,summaries_dict = summaries_dict,summarizer="test")
            # **** end testing ****
            
            #saving
            if f1>self.max_f1:
                self.model.save(self.sess)
                self.max_f1 = f1
            
    def train_step(self):
        feed_dict = {self.data.handle: self.data.training_handle, self.model.is_training : True}                       
        _ , loss,predicted_classes, correct_classes,lr = self.sess.run([self.model.train_step,
                                                                             self.model.loss,
                                                                             self.model.predictions,
                                                                             self.model.classes,
                                                                             self.model.learning_rate], feed_dict = feed_dict)
        return loss,predicted_classes,correct_classes,lr
        
    def test_step(self):
        feed_dict = {self.data.handle: self.data.testing_handle,self.model.is_training : True}     
        loss,predicted_classes,correct_classes = self.sess.run([self.model.loss, self.model.predictions, self.model.classes], feed_dict = feed_dict)
        return loss,predicted_classes,correct_classes

    
    def precision_recall_f1_accuracy(self,labels,predictions):
        uniques = np.unique(labels)
        #number of classes
        n = uniques.shape[0]
        precision = []
        accuracy = []
        recall = []
        f1 = []
        for k in range(n):
            cur_label = uniques[k]
            tp = ((labels == cur_label) & (predictions == cur_label)).sum()
            fp = ((labels != cur_label) & (predictions == cur_label)).sum()
            fn = ((labels == cur_label) & (predictions != cur_label)).sum()
            tn = ((labels != cur_label) & (predictions != cur_label)).sum()
            if tp + fp != 0:
                recall.append(tp/(tp + fn))
                if tp == 0:
                    precision.append(0)
                    f1.append(0)
                else:
                    precision.append(tp/(tp + fp))
                    f1.append(2*precision[-1]*recall[-1]/(precision[-1] + recall[-1]))
            accuracy.append((tp+tn)/(tp+fp+fn+tn))
        precision = np.array(precision)
        recall = np.array(recall)
        f1 = np.array(f1)
        accuracy = np.array(accuracy)
        accuracy = np.mean(labels == predictions)
        return np.mean(precision), np.mean(recall), np.mean(f1), np.mean(accuracy)