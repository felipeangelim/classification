Created by Felipe Vieira and Sebastien Poullot

Some things to keep in mind:

- simple.py is a wrapper with the objective to simplify training of the predefined deep learning models.
- This git does not include pretrained models (they're too heavy). Please download them from Tensorflow SLIM github page and check if the file's names are coherent with the ones in the code.
- To reuse a personal pretrained model, uncomment the model.load() line in simple.py.
- When simple.py script is used, a .txt file with the path to each file is created. By default, this file is reused. If you want to train the network with new data or a new data_dir, please delete both train and test .txt files before lauching it.

