# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 12:07:15 2018

@author: fva3
"""

import os
import time
from collections import Counter

#params
path = "F:/Datasets/dodeca45/test/"
n_views = 20

start = time.clock()

pngs_paths = []
for root, dirs, files in os.walk(path, topdown=False):
   for name in files:
      if name.endswith('png'):
          pngs_paths.append(root)

counter = Counter(pngs_paths)

objs_with_errors = []
for key in counter.keys():
    if counter[key] < n_views: 
        objs_with_errors.append(key)

end = time.clock()
print(end - start, ' sec ')
print(objs_with_errors)
