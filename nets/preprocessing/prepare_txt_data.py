# -*- coding: utf-8 -*-
"""
Created on Mon Aug 13 15:25:21 2018

@author: FVA3
"""

import os
from collections import Counter
import numpy as np
from random import randint


def prepare_txt_data(all_dir = 'C:/tmp/demoXcom/WhiteBack20/',
                     path_out_train = 'train.txt',
                     path_out_test = 'test.txt',
                     n_views = 20, objs_per_class = 400,
                     shuffle_patch_size = 1,
                     label_index = -2,
                     verbose = False):
    """
    Generate balanced and shuffled train text file and a test text file.
    Inputs:
        - all_dir: directory that contains the 'train' and 'test' folder with the data
        - path_out_train:
        - path_out_test: 
        - n_views: number of views per object
        - objs_per_class: number of objects per class for balacing purposes
        - shuffle_patch_size: number of objects to shuffle together (for e.g., it could be
         equal to the number of views if we are interested in keeping all the views of the same
         object together)
    Outputs:
        - Generate two .txt files named path_out_train and path_out_test
    
    """
    
    train_dir = all_dir + 'train/'
    test_dir = all_dir + 'test/'
    
    print('1/3 -- reading dataset')
    
    label_index = -2
    f_out = open(path_out_train,'w')
    
    labels = []
    all_objs = {}
    objs_to_write = []
    for root, dirs, files in os.walk(train_dir, topdown=False):
       for filename in files:
          if filename.endswith('.png'):
              path = os.path.join(root, filename).replace('\\','/')
              label = path.split('/')[label_index]
              objs_to_write.append(path)
              labels.append(label)
              try:
                  all_objs[label].append(path)
              except:
                  all_objs[label] = [path]
                
                
    
    print('2/3 -- balancing dataset')
    
    labels = np.array(labels)
    counter = Counter(labels)
    if verbose:
        print(counter)
        print('Size before balacing: ', len(objs_to_write))
    for label in counter:
        if counter[label] < objs_per_class*n_views:
            paths = all_objs[label]
            try:
                assert(len(paths)//n_views == counter[label])
            except:
                print('WARNING, number of images is not a multiple of the number of views!')
            for i in range(objs_per_class*n_views-counter[label]):
                index = i%counter[label]
                objs_to_write.append(paths[index])
    if verbose:
        print('Size after balacing: ', len(objs_to_write))
    print('3/3 -- Shuffling train set...')
        
    groups_of_objs = []
    for i in range(0,len(objs_to_write),shuffle_patch_size):
        groups_of_objs.append(objs_to_write[i:i+shuffle_patch_size])
    
        
    groups_of_objs = np.array(groups_of_objs)
    np.random.shuffle(groups_of_objs)
    
    groups_of_objs = groups_of_objs.flatten()
    
    for i in range(groups_of_objs.shape[0]):
        f_out.write(groups_of_objs[i] +'\n')
    
    f_out.close()
    
    f_out_test = open(path_out_test,'w')
    for root, dirs, files in os.walk(test_dir, topdown=False):
       for filename in files:
          if filename.endswith('.png'):
              f_out_test.write(os.path.join(root, filename).replace('\\','/') + '\n')      
    f_out_test.close()



    print('Done')
    