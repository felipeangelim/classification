config_rotnet = {
          "objs_per_class" : 400,
          "shuffle_patch_size" : 20,
          "views_num":20,
          "classes_num": 10,
          "optimizer": "momentum",
          "momentum": 0.9,
          "learning_rate": 0.1,
          "learning_rate_bottom_top_ratio": 0.01,
          "weight_decay": 0.0005,
          "decay_factor": 0.9,
          "num_epochs_per_decay": 1,
          "keep_prob": 0.65,
          "epochs": 80,
          "batch_size": 100,
          "max_to_keep":1,
          "vars_to_train":2,
          "model": "inception_resnet_v2",
          "weights_path": "pretrained/inception_resnet_v2.ckpt",
          "data_dir": "C:/tmp/demoXcom/modelnet10/",
          "label_index": -3,
          "checkpoint_dir": "checkpoints/",
          "summary_dir": "summary/",
          "tests_per_epoch": 1,
          "rotationnet_case": 2
          }

config_cnn = {
          "objs_per_class" : 400,
          "shuffle_patch_size" : 1,
          "views_num":20,
          "classes_num": 10,
          "optimizer": "momentum",
          "momentum": 0.9,
          "learning_rate": 0.1,
          "weight_decay": 0.0005,
          "decay_factor": 0.9,
          "num_epochs_per_decay": 1,
          "keep_prob": 0.65,
          "epochs": 80,
          "batch_size": 100,
          "max_to_keep":1,
          "vars_to_train":2,
          "model": "mobilenet_v1_1.0_224",
          "weights_path": "pretrained/mobilenet_v1_1.0_224.ckpt",
          "data_dir": "C:/tmp/demoXcom/modelnet10/",
          "label_index": -3,
          "checkpoint_dir": "checkpoints/",
          "summary_dir": "summary/",
          "tests_per_epoch": 1,
          }

config_shallow = {
          "learning_rate": 0.1,
          "epochs": 80,
          "batch_size": 400,
          "max_to_keep":1,
          "checkpoint_dir": "checkpoints/",
          "summary_dir": "summary/"
          }


import tensorflow as tf
import numpy as np
import os 

class RotationNet:
    def __init__(self,data_dir = None,classes_num = None, **kargs):
        """
        Inputs:
            - data_dir: directory with train and test folders
            - kargs: optional arguments. By default, they're

                    "objs_per_class" : 400,
                    "views_num":20,
                    "classes_num": 10,
                    "optimizer": "momentum",
                    "momentum": 0.9,
                    "learning_rate": 0.1,
                    "weight_decay": 0.0005,
                    "decay_factor": 0.9,
                    "num_epochs_per_decay": 1,
                    "keep_prob": 0.65,
                    "epochs": 80,
                    "batch_size": 100,
                    "max_to_keep":1,
                    "vars_to_train":2,
                    "model": "inception_resnet_v2",
                    "weights_path": "pretrained/inception_resnet_v2.ckpt",
                    "data_dir": "D:/tmp/demoXcom/modelnet10/",
                    "label_index": -3,
                    "checkpoint_dir": "checkpoints/",
                    "summary_dir": "summary/",
                    "tests_per_epoch": 1,
                    "rotationnet_case": 2
        """
        from RotationNet.Model import RotationNet as CNN
        from RotationNet.Dataloader import Data
        from RotationNet.Trainer import Trainer
        from RotationNet.Logger import Logger
        self.Model = CNN
        config_rotnet["data_dir"] = data_dir
        config = config_rotnet
          
        for key in kargs:
            if key not in config.keys():
                print('Key "',key,'" is not an argument.')
                raise ValueError
            config[key] = kargs[key]
        
        if classes_num == None or data_dir == None:
            raise ValueError("Must specify the number of classes")
            
        config['classes_num'] = classes_num
        def write_label_txt(encoding):
            f = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'class_labels.txt'), 'w')
            try:
                first = True
                for key in encoding.keys():
                    if first:
                        first = False
                    else:
                        f.write('\n')
                    f.write(str(encoding[key]))
            finally:
                f.close()
        
        configProto = tf.ConfigProto(allow_soft_placement = True)
        configProto.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
        configProto.log_device_placement = True 
        #create tensorflow session
        self.sess = tf.Session(config = configProto)
        #data class
        self.data = Data('train_rotNet.txt','test_rotNet.txt',config,self.sess, verbose = False)
        #write class_labels.txt file
        write_label_txt(self.data.label_encoder.i_encoding)
        #defining model
        self.model = CNN(self.data,config)
        #loading initial weights
        self.model.load_initial_weights(self.sess)
        #initializing variables that aren't in the pretrained checkpoint
        self.model.initialize_uninitialized(self.sess)
        #self.model.load(self.sess)
        #logger
        self.logger = Logger(self.sess,config)
        #trainer
        self.trainer = Trainer(self.sess, self.model, self.data, config, self.logger)
        
        self.config = config

    def train(self):
        self.trainer.train()

class CNN:
    def __init__(self,data_dir = None,  deploy = False, **kargs):
        """
        Inputs:
            - data_dir: directory with train and test folders
            - kargs: optional arguments. By default, they're

                    "objs_per_class" : 400,
                    "shuffle_patch_size" : 1,
                    "views_num":20,
                    "classes_num": 10,
                    "optimizer": "momentum",
                    "momentum": 0.9,
                    "learning_rate": 0.1,
                    "weight_decay": 0.0005,
                    "decay_factor": 0.9,
                    "num_epochs_per_decay": 1,
                    "keep_prob": 0.65,
                    "epochs": 80,
                    "batch_size": 100,
                    "max_to_keep":1,
                    "vars_to_train":2,
                    "model": "inception_resnet_v2",
                    "weights_path": "pretrained/inception_resnet_v2.ckpt",
                    "data_dir": "D:/tmp/demoXcom/modelnet10/",
                    "label_index": -3,
                    "checkpoint_dir": "checkpoints/",
                    "summary_dir": "summary/",
                    "tests_per_epoch": 5,
                    "rotationnet_case": 2
        """
        from CNN.Model import CNN
        from CNN.Dataloader import Data
        from CNN.Trainer import Trainer
        from CNN.Logger import Logger
        self.Model = CNN

        config_cnn["data_dir"] = data_dir
        config = config_cnn
        
        for key in kargs:
            if key not in config.keys():
                print('Key "',key,'" is not an argument.')
                raise ValueError
            config[key] = kargs[key]
        

        def write_label_txt(encoding):
            f = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'class_labels.txt'), 'w')
            try:
                first = True
                for key in encoding.keys():
                    if first:
                        first = False
                    else:
                        f.write('\n')
                    f.write(str(encoding[key]))
            finally:
                f.close()
        
        configProto = tf.ConfigProto(allow_soft_placement = True)
        #create tensorflow session
        self.sess = tf.Session(config = configProto)
        #data class
        self.data = Data('train_cnn.txt','test_cnn.txt',config,self.sess, verbose = False)
        #write class_labels.txt file
        write_label_txt(self.data.label_encoder.i_encoding)

        #defining model
        self.model = CNN(self.data,config, deploy = deploy)
        #loading initial weights
        self.model.load_initial_weights(self.sess)
        #initializing variables that aren't in the pretrained checkpoint
        self.model.initialize_uninitialized(self.sess)
        #logger
        self.logger = Logger(self.sess,config)
        #trainer
        self.trainer = Trainer(self.sess, self.model, self.data, config, self.logger)
        
        self.config = config

    def train(self):
        self.trainer.train()

class Shallow:
    def __init__(self,x_train, y_train, x_test, y_test, **kargs):
        """
        Inputs:
            - data_dir: directory with train and test folders
            - kargs: optional arguments. By default, they're

                    "learning_rate": 0.1,
                    "epochs": 80,
                    "batch_size": 400,
                    "max_to_keep":1,
                    "checkpoint_dir": "checkpoints/",   
                    "summary_dir": "summary/",
                    "tests_per_epoch": 5,
        """
        from CNN.Model import ClassifierNet
        from CNN.Dataloader import Data
        from CNN.Trainer import Trainer
        from CNN.Logger import Logger
        
        config = config_shallow
        
        for key in kargs:
            if key not in config.keys():
                print('Key "',key,'" is not an argument.')
                raise ValueError
            config[key] = kargs[key]
        
        configProto = tf.ConfigProto(allow_soft_placement = True)
        #create tensorflow session
        self.sess = tf.Session(config = configProto)
        #data class
        self.data = Data( x_train, y_train, x_test, y_test, config)
        #defining model
        self.model = ClassifierNet(x_train.shape[1], np.unique(y_train).shape[0],config)
        #logger
        self.logger = Logger(self.sess,config)
        #trainer
        self.trainer = Trainer(self.sess, self.model, self.data, config, self.logger)
        self.config = config
        

    def train(self):
        self.trainer.train()