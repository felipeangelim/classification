# -*- coding: utf-8 -*-
"""
Created on Sun Jun  3 13:52:23 2018

@author: felip
"""

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tqdm import tqdm
import os
from tensorflow.contrib import slim

class CNN():
    def __init__(self, data,config, deploy = False):
        self.config = config
        self.data = data
        
        self.init_global_step()
        
        #parameters
        self.CLASSES_NUM = config['classes_num']
        self.VIEWS_NUM = config['views_num']
        self.OPTIMIZER = config['optimizer'].lower()
        self.MOMENTUM = config['momentum']
        self.LEARNING_RATE = config['learning_rate']
        self.KEEP_PROB = config['keep_prob']
        self.WEIGHTS_PATH = config["weights_path"]
        self.MODEL = config["model"].lower()
        self.BATCH_SIZE = config["batch_size"]
        self.VARS_TO_TRAIN = config["vars_to_train"]
        self.DECAY_FACTOR = config['decay_factor']
        self.NUM_EPOCHS_PER_DECAY = config['num_epochs_per_decay']
        self.OBJS_PER_BATCH = int(self.data.BATCH_SIZE/self.VIEWS_NUM)
        # init the epoch counter
        self.init_cur_epoch()
        #build the network
        self.build_network(deploy)
        #initialize saver
        self.init_saver()
        #create checkpoint directory if it doesn't exist
        self.checkpoint_dir = config['checkpoint_dir']
        if not os.path.exists(self.checkpoint_dir):
            os.makedirs(self.checkpoint_dir)
            
    def build_network(self,deploy):
        ''' 
        Build the model graph 
        
        '''
        #boolean
        self.is_training = tf.placeholder(tf.bool)
        
        #get data from Input Pipeline
        if deploy:
            self.x = tf.placeholder(tf.float32, [1,224,224,3], name = 'image')
            self.y = tf.placeholder(tf.int32, [1,self.CLASSES_NUM])
        else:
            self.x, self.y = self.data.next_element
        
        #chosing the right model
        #here we specify if we want extra layers after the base CNN
        #is_training is set to False because we manually compute the gradients
        #all networks implemented in TensorFlow Slim can be used here
        #for more informations about tensorflow slim, please check their github
        
        #### Inception - Resnet MODEL ####
        if self.MODEL == "inception_resnet_v2":
            import base_models.inception_resnet_v2 as inception_resnet_v2
            with slim.arg_scope(inception_resnet_v2.inception_resnet_v2_arg_scope()):
                self.logits, self.end_points  = inception_resnet_v2.inception_resnet_v2(self.x,
                                                                                        num_classes = self.CLASSES_NUM,
                                                                                        dropout_keep_prob = self.KEEP_PROB,
                                                                                        create_aux_logits=True,
                                                                                        is_training= False)
        #### MobileNet v1 MODEL ####
        elif self.MODEL == "mobilenet_v1_1.0_224":
            import base_models.mobilenet_v1 as mobilenet_v1
            with slim.arg_scope(mobilenet_v1.mobilenet_v1_arg_scope()):
                
                self.logits, self.end_points  = mobilenet_v1.mobilenet_v1(self.x,
                                                                        num_classes = self.CLASSES_NUM,
                                                                        dropout_keep_prob = self.KEEP_PROB,
                                                                        is_training = False,
                                                                        prediction_fn = None,
                                                                        global_pool = True,
                                                                        final_endpoint = 'Conv2d_13_pointwise')
        #### Inception V1 Model ####
        elif self.MODEL == "inception_v1":
            import base_models.inception_v1 as inception_v1
            with slim.arg_scope(inception_v1.inception_v1_arg_scope()):
                self.logits, self.end_points  = inception_v1.inception_v1(self.x,
                                                                        num_classes = 1024,
                                                                        dropout_keep_prob = self.KEEP_PROB,
                                                                        is_training = False,
                                                                        prediction_fn = None)                
        else:
            raise ValueError('Model not available.')
    
        # Train op
        with tf.name_scope("train"):
            #loss function
            with tf.name_scope("loss"):
                self.probabilities = tf.nn.softmax(self.logits)
                self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits( labels = self.y, logits = self.logits))
                self.predictions = tf.argmax(self.logits, axis = -1)
                self.classes = tf.argmax(self.y, axis = -1)
                self.good_prediction_bool_tensor = tf.cast(tf.equal(self.classes,self.predictions),dtype =tf.float32)
                self.accuracy = tf.reduce_mean(self.good_prediction_bool_tensor)
                        
            # Create optimizer and apply gradient descent to the trainable variables
            # We apply different optimization methods for each case
            num_epochs_per_decay = self.NUM_EPOCHS_PER_DECAY
            decay_factor = self.DECAY_FACTOR
            decay_steps = int(self.data.train_size / self.BATCH_SIZE * num_epochs_per_decay)
            self.learning_rate = tf.train.exponential_decay(self.LEARNING_RATE,
                                                            self.global_step_tensor,
                                                            decay_steps,
                                                            decay_factor,
                                                            staircase=True)
            if self.OPTIMIZER == 'momentum':
                optimizer_top = tf.train.MomentumOptimizer(self.LEARNING_RATE,self.MOMENTUM)
                optimizer_bottom = tf.train.MomentumOptimizer(self.learning_rate/100,self.MOMENTUM)
            elif self.OPTIMIZER == "rmsprop":                
                optimizer_top = tf.train.RMSPropOptimizer(self.learning_rate,decay = 0.9,momentum = self.MOMENTUM,epsilon= 1e-10)
                optimizer_bottom = tf.train.RMSPropOptimizer(self.learning_rate/100,decay = 0.9,momentum = self.MOMENTUM)
            elif self.OPTIMIZER == 'adam':
                optimizer_top = tf.train.AdamOptimizer(self.learning_rate)
                optimizer_bottom = tf.train.AdamOptimizer(self.learning_rate/100)
            else:
                raise NotImplementedError
            
            # Here we chose which variables we'd like to train
            var_list = [v for v in tf.trainable_variables()][-self.VARS_TO_TRAIN:]
            print('[CNN]', len(var_list), 'trainable variables')
            print('[CNN] Training from', var_list[0], 'up to', var_list[-1])
            # setting different training ops for each part of the network
            # we set a smaller lr for the layers in the middle
            var_list_bottom = [v for v in tf.trainable_variables()][-8:-self.VARS_TO_TRAIN]
            gradients = tf.gradients(self.loss, var_list_bottom + var_list)
            grads_bottom = gradients[:-self.VARS_TO_TRAIN]
            grads_top = gradients[-self.VARS_TO_TRAIN:]
            train_op_bottom = optimizer_bottom.apply_gradients(zip(grads_bottom, var_list_bottom), global_step = self.global_step_tensor)
            train_op_top = optimizer_top.apply_gradients(zip(grads_top, var_list))
            self.train_step = tf.group(train_op_bottom, train_op_top)
  
        
    def load_initial_weights(self, sess):
        """
        Load weights from file into network.
        """
        
        pretrained_checkpoint = self.WEIGHTS_PATH
        #we exclude some layers we don't want for example the softmax layer of the original CNN
        if self.MODEL ==    "inception_resnet_v2":
            checkpoint_exclude_scopes=["InceptionResnetV2/Logits", "InceptionResnetV2/AuxLogits", "fully_connected_1","fully_connected"]
        if self.MODEL == "mobilenet_v1_1.0_224":
            checkpoint_exclude_scopes=["MobilenetV1/Logits", "MobilenetV1/AuxLogits", "fully_connected_1","fully_connected"]
        else:
            checkpoint_exclude_scopes = ["fully_connected_1","fully_connected"]
        if self.MODEL == "inception_v1":
            checkpoint_exclude_scopes=["InceptionV1/Logits", "InceptionV1/AuxLogits","fully_connected_1","fully_connected"]
        
    
        exclusions = [scope.strip() for scope in checkpoint_exclude_scopes]
        
        variables_to_restore = []
        for var in slim.get_model_variables():
            for exclusion in exclusions:
                if var.op.name.startswith(exclusion):
                    break
            else:
                variables_to_restore.append(var)
                
        print("Restoring variables up to", var)
        init_fn = slim.assign_from_checkpoint_fn(pretrained_checkpoint,variables_to_restore)
        init_fn(sess)
                    
    def initialize_uninitialized(self,sess):
        """ 
        This function initialize variables that weren't initialized by loading the weights
        
        """
        global_vars          = tf.global_variables()
        is_not_initialized   = sess.run([tf.is_variable_initialized(var) for var in global_vars])
        not_initialized_vars = [v for (v, f) in zip(global_vars, is_not_initialized) if not f]
        if len(not_initialized_vars):
            sess.run(tf.variables_initializer(not_initialized_vars))     
            
    def init_saver(self):
        """
        Init saver
        If you want to save all variables, please hide var_list input
        
        """
        tf.global_variables_initializer()
        self.saver = tf.train.Saver(max_to_keep=self.config['max_to_keep'])
    
    def init_global_step(self):
        # DON'T forget to add the global step tensor to the tensorflow trainer
        with tf.variable_scope('global_step'):
            self.global_step_tensor = tf.Variable(0, trainable=False, name='global_step')
    
    def init_cur_epoch(self):
        with tf.variable_scope('cur_epoch'):
            self.cur_epoch_tensor = tf.Variable(0, trainable=False, name='cur_epoch')
            self.increment_cur_epoch_tensor = tf.assign(self.cur_epoch_tensor, self.cur_epoch_tensor + 1)
            
    
    def save(self, sess):
        """
        Save model checkpoint in checkpoint dir
        
        """
        print("Saving model...")
        self.saver.save(sess, self.checkpoint_dir, self.global_step_tensor)
        print("Model saved")
    
    def load(self, sess):
        """
        Load last saved model from checkpoint path
        
        """
        latest_checkpoint = tf.train.latest_checkpoint(self.checkpoint_dir)
        if latest_checkpoint:
            print("Loading model checkpoint {} ...\n".format(latest_checkpoint))
            self.saver.restore(sess, latest_checkpoint)
            print("Model loaded from {}".format(latest_checkpoint))

    def load_trainable(self,sess):
        tf.global_variables_initializer()
        self.saver = tf.train.Saver(var_list = tf.trainable_variables(), max_to_keep=self.config['max_to_keep'])
        self.load(sess)
        
        
    def load_path(self, sess, path):
        """
        Load model specified by path
        """
        print("Loading model checkpoint {} ...\n".format(path))
        self.saver.restore(sess, path)
        print("Model loaded")