# -*- coding: utf-8 -*-
"""
Created on Sun Jun  3 14:52:54 2018

@author: felip
"""

import numpy as np
from collections import Counter
import os
from tensorflow.python.framework.ops import convert_to_tensor
from tensorflow.python.framework import dtypes
from preprocessing.prepare_txt_data import prepare_txt_data
import tensorflow as tf

class Data():
    
    def __init__(self,
                 train_txt,
                 test_txt,
                 config,
                 sess,
                 seed = 42,
                 verbose = False,
                 shuffle_buffer_size = 1000,
                 prefetch_buffer_size = 100):
        tf.set_random_seed(seed)
        self.seed = seed
        '''
        Inputs:
            train_txt: path to .txt file containing the path to training images
            test_txt: path to .txt file containing the path to testing images
            config: dict with config options
            sess: TensorFlow session
            reuse: wheter to reuse the .txt files (train_txt, test_txt)
            seed: tensorflow seed for random operations
            shuffle_buffer_size: buffer size for data shuffling
            pretech_buffer_size: buffer size for prefetching the dataset
        '''
        
        #setting the parameters defined in the config
        self.config = config
        self.data_dir = config['data_dir']
        self.test_dir = self.data_dir + 'test/'
        self.train_dir = self.data_dir + 'train/'
        self.train_txt = train_txt
        self.test_txt = test_txt
        self.sess = sess
        self.OBJS_PER_CLASS = config['objs_per_class']
        self.SHUFFLE_PATCH_SIZE = config['shuffle_patch_size']
        self.CLASSES_NUM = config['classes_num']
        self.BATCH_SIZE = config['batch_size']
        self.VIEWS_NUM = config['views_num']
        self.LABEL_INDEX = config['label_index']
        
        
        reuse = os.path.isfile(train_txt) and os.path.isfile(test_txt) 
        if not reuse:
            prepare_txt_data(all_dir = self.data_dir,
                             path_out_train = train_txt,
                             path_out_test = test_txt,
                             n_views = self.VIEWS_NUM,
                             objs_per_class = self.OBJS_PER_CLASS,
                             shuffle_patch_size = self.SHUFFLE_PATCH_SIZE,
                             label_index = config['label_index'],
                             verbose = verbose)
        
        #array of labels
        self.unique_labels = np.array([name for name in os.listdir(self.train_dir) if os.path.isdir(os.path.join(self.train_dir, name))])
        assert(len(self.unique_labels) == self.CLASSES_NUM)
        
        #create label encoder
        self.label_encoder =  LabelEncoder()
        self.label_encoder.fit(self.unique_labels)
        
        #read txt_files
        self.x_train, self.y_train = self._read_txt_file(train_txt)
        self.x_test, self.y_test = self._read_txt_file(test_txt)        
        self.test_size = len(self.y_test)
        self.train_size = len(self.y_train)
        
        #define number of batchs for each epoch        
        self.num_batch = (len(self.x_train)//self.BATCH_SIZE)
        if (self.train_size%self.BATCH_SIZE) != 0:
            # if training set size is not a multiple of batch size
            self.num_batch += 1
        
        #convert lists to TF tensor
        self.x_train = convert_to_tensor(self.x_train, dtype = dtypes.string)
        self.x_test = convert_to_tensor(self.x_test, dtype = dtypes.string)
        self.y_train = convert_to_tensor(self.y_train, dtype = dtypes.int32)
        self.y_test = convert_to_tensor(self.y_test, dtype = dtypes.int32)
        
        
        #Create train dataset                
        self.x_train = tf.data.Dataset.from_tensor_slices(self.x_train)
        self.y_train = tf.data.Dataset.from_tensor_slices(self.y_train)
        self.train_dataset = tf.data.Dataset.zip((self.x_train,self.y_train))
        
        #Create test dataset
        self.x_test = tf.data.Dataset.from_tensor_slices(self.x_test)
        self.y_test = tf.data.Dataset.from_tensor_slices(self.y_test)
        self.test_dataset = tf.data.Dataset.zip((self.x_test,self.y_test))
        
        #mapping the datasets(for e.g. converting the image path to an array)
        self.train_dataset = self.train_dataset.map(self._parse_function_train, num_parallel_calls= 7)
        self.test_dataset = self.test_dataset.map(self._parse_function_test, num_parallel_calls= 6)
        
        #Here we shuffle the dataset, repeat it so it can be automatically reinitialized,
        #we separate it into batches of self.BATCH_SIZE and then we prefetch it (to speed data preprocessing)
        #Please, refer to Tensorflow Dataset API for more information
        #.shuffle(shuffle_buffer_size)
        self.train_dataset = self.train_dataset.repeat().batch(self.BATCH_SIZE).prefetch(prefetch_buffer_size)
        self.test_dataset = self.test_dataset.repeat().batch(self.BATCH_SIZE).prefetch(prefetch_buffer_size)
        
        #We create a feedable iterator
        self.handle = tf.placeholder(tf.string, shape=[])
        iterator = tf.data.Iterator.from_string_handle(self.handle,
                                                       self.train_dataset.output_types,
                                                       self.train_dataset.output_shapes)
        self.next_element = iterator.get_next()
        
        #training iterator
        self.training_iterator = tf.data.Iterator.from_structure(self.train_dataset.output_types,
                                               self.train_dataset.output_shapes)  
        
        #testing iterator
        self.testing_iterator = tf.data.Iterator.from_structure(self.train_dataset.output_types,
                                               self.train_dataset.output_shapes)  
        
        self.training_handle = self.sess.run(self.training_iterator.string_handle())
        self.testing_handle = self.sess.run(self.testing_iterator.string_handle())
        
        #init operations 
        self.training_init_op = self.training_iterator.make_initializer(self.train_dataset)
        self.testing_init_op = self.testing_iterator.make_initializer(self.test_dataset)
        
    
    def _parse_function_train(self,filename,classes):
        """
        This function parses each element of the training set
        """
        one_hot = classes
        
        #load and preprocess the image
        img_string = tf.read_file(filename)
        #decode png image
        img_decoded = tf.image.decode_png(img_string, channels=3)
        #do data augmentation
        img_resized = self._data_augmentation(img_decoded)
        #convert to 0 - 1 float
        img = tf.image.convert_image_dtype(img_resized,tf.float32)/255
        img = tf.subtract(img, 0.5)
        img = tf.multiply(img, 2.0)
        
        return img,one_hot
    
    def _parse_function_test(self,filename,classes):
        """
        This function parses each element of the testing set
        """
        one_hot = classes
        
        # load and preprocess the image
        img_string = tf.read_file(filename)
        #decode png image
        img_decoded = tf.image.decode_png(img_string, channels=3)
        #resize the image
        img_resized = tf.image.resize_images(img_decoded, [224, 224])
        #convert to 0 - 1 float
        img = tf.image.convert_image_dtype(img_resized,tf.float32)/255
        img = tf.subtract(img, 0.5)
        img = tf.multiply(img, 2.0)

        return img,one_hot

    
    def _data_augmentation(self, img):
        """ 
        Define the data augmentation procedure
        Inputs:
            - RGB image before resizing
        Outputs:
            - Resized and augmented image 
        
        """
        
        #img_resized = tf.image.random_hue(img,0.5,self.seed)
        
        def to_grayscale(img):
            img_resized = tf.image.rgb_to_grayscale(img)
            img_resized = tf.image.grayscale_to_rgb(img_resized)
            return img_resized
        
        def constrast_brightness(img):
            img =tf.cond(tf.less(tf.random_uniform(shape=[], minval=0, maxval=1, dtype=tf.float32), 0.5),
                    true_fn = lambda: tf.image.random_contrast(tf.image.random_brightness(img, 0.25, self.seed), 0.5, 1.5),
                    false_fn = lambda: tf.image.random_brightness(tf.image.random_contrast(img, 0.5, 1.5), 0.25, self.seed))
            return img
        
        def do_nothing(img):
            return img
        
        img = tf.cond(tf.less(tf.random_uniform(shape=[], minval=0, maxval=1, dtype=tf.float32), 0.5),
                true_fn = lambda: constrast_brightness(img),
                false_fn = lambda: do_nothing(img))
        
        img = tf.cond(tf.less(tf.random_uniform(shape=[], minval=0, maxval=1, dtype=tf.float32), 0.5),
                true_fn = lambda: to_grayscale(img),
                false_fn = lambda: do_nothing(img))
        
        img = tf.image.random_flip_left_right(img,seed = self.seed)
        img = tf.image.random_flip_up_down(img,seed = self.seed)
        
        #random crop image
        img_resized = tf.image.resize_images(img, [300,300])
        img_resized = tf.random_crop(img_resized,[224,224,3],seed = self.seed)
        return img_resized    
    
    def _read_txt_file(self,txt_file):
        """
        Read the content of the text file and store it into lists
        Inputs:
            - txt_file: the path to the .txt file with the image paths
        Outputs:
            - img_paths: a list with the paths to the input images
            - classes: a list with one-hot encoded classes
        """
        img_paths = []
        classes = []
        print("Reading text file...")
        with open(txt_file, 'r') as f:
            lines = f.readlines()
            for line in lines:
                img_paths.append(line[:-1])
                class_name = line.split('/')[self.LABEL_INDEX]
                class_number = self.label_encoder.transform([class_name])[0]
                classes.append(self.label_encoder.one_hot(class_number, self.CLASSES_NUM))
        return img_paths,classes

class LabelEncoder:
    def __init__(self):
        pass
    
    def fit(self,y):
        y = np.array(y)
        self.uniques = np.unique(y)
        self.size = self.uniques.shape[0]
        self.i_encoding = {}
        self.encoding = {}
        for i in range(self.size):
            self.i_encoding[i] = self.uniques[i]
            self.encoding[self.uniques[i]] =  i
    def transform(self,array):
        array = np.array(array)
        new_array = []
        for i in range(array.shape[0]):
            new_array.append(self.encoding[array[i]])
        return np.array(new_array)
    
    def inverse_transform(self,array):
        array = np.array(array)
        new_array = []
        for i in range(array.shape[0]):
            new_array.append(self.i_encoding[array[i]])
        return np.array(new_array)
    
    def add(self,label):
        self.i_encoding[self.size] = label
        self.encoding[label] = self.size
        self.size += 1
    
    def one_hot(self,number,depth):
        return [1 if i == number else 0 for i in range(depth)]