{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tensorflow Model Tuning and Conversion to CoreML"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we'll use a object-oriented approach to build and train a TensorFlow Model. Many state-of-the-art methods for 3D object recognition can be implemented based on this code, with minimal alterations.\n",
    "\n",
    "Finally, the trained model will be converted to .pb and coreML model, two models that are suited for Android and iOS applications, respectively.\n",
    "\n",
    "## Table of contents\n",
    "\n",
    "- <a href='#gettingstarted'>1. Getting Started</a>\n",
    "    - <a href='#prerequisites'>1.1. Prerequisites</a>\n",
    "    - <a href='#folders'>1.2. Folder structure</a>\n",
    "        - <a href='#cnn'>1.2.1. CNN.py</a>\n",
    "        - <a href='#trainer'>1.2.2. Trainer.py</a>\n",
    "        - <a href='#data'>1.2.3. Data.py</a>\n",
    "        - <a href='#logger'>1.2.4. Logger.py</a>\n",
    "- <a href='#practices'>2. Best practices</a>\n",
    "    - <a href='#tuning'>2.1. Hyperparameter tuning</a>\n",
    "    - <a href='#tensorboard'>2.2. Tensorboard</a>\n",
    "- <a href='#training'>3. Training the model</a>\n",
    "- <a href='#freezing'>4. Freezing the graph</a>\n",
    "- <a href='#converting'>5. Converting to CoreML</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='gettingstarted'></a>\n",
    "## 1. Getting started \n",
    "****\n",
    "\n",
    "<a id='prerequisites'></a>\n",
    "### 1.1. Prerequisites\n",
    "***\n",
    "\n",
    "- tensorflow >= 1.8\n",
    "- [tqdm] (https://github.com/tqdm/tqdm)\n",
    "- Numpy\n",
    "- Matplotlib\n",
    "- tfcoreml\n",
    "- coremltools (pip install git+https://github.com/apple/coremltools)\n",
    "\n",
    "<a id='folders'></a>\n",
    "### 1.2. Folder structure \n",
    "***\n",
    "\n",
    "The folders are organized as follows:\n",
    "\n",
    "    - main.ipynb\n",
    "    - models/\n",
    "        └── CNN.py  # here the model is defined\n",
    "    - trainers/\n",
    "        └── Trainer.py # the Trainer class specifies the training schedule\n",
    "    - dataloader/\n",
    "        └── Data.py # responsible for general preprocessing of the data\n",
    "    - utils/\n",
    "        └── Logger.py # manages the training and test summary (useful for analysing data with Tensorboard)\n",
    "            ...\n",
    "<a id='cnn'></a>\n",
    "#### 1.2.1. CNN.py\n",
    "\n",
    "This file defines the model to be fine-tuned. It's can useful to add extra layers to the model if the task is complicated and if there's the enough data, or you can want to use a specific layer as a feature extractor. This can be easily done in the build_network() function.\n",
    "\n",
    "Three CNN models are supported for fine-tuning and transfer learning: Inception-Resnet, Inception V1, MobileNet. This implementation is based on [TensorFlow Slim](https://github.com/tensorflow/tensorflow/tree/master/tensorflow/contrib/slim).\n",
    "\n",
    "<a id='trainer'></a>\n",
    "#### 1.2.2. Trainer.py\n",
    "\n",
    "The Trainer class organizes the training procedure. The training and testing steps are written in this file.\n",
    "\n",
    "<a id='data'></a>\n",
    "#### 1.2.3. Data.py\n",
    "\n",
    "It's often desirable to test different data augmentation techniques to see which one better fits the problem. The operations should be specified inside the _data_augmentation() function or inside _parse_function_train().\n",
    "\n",
    "<a id='logger'></a>\n",
    "#### 1.2.4. Logger.py\n",
    "\n",
    "This class registers variables so they can be read through Tensorboard.\n",
    "\n",
    "<a id='practices'></a>\n",
    "## 2. Best practices\n",
    "***\n",
    "\n",
    "<a id='tuning'></a>\n",
    "### 2.1. Hyperparameter Tuning  \n",
    "\n",
    "The model may need to be tuned differently for each application. The parameters we need to adjust are named \"Hyperparameters\". Some examples are: learning rate, batch size, decay rate, momentum, dropout rate. Altought there is no exact rule for hyperparameter tuning, some rules of thumb can help.\n",
    "\n",
    "Smaller batches are usually prefered. Generalization error tends to be lower for batches of size 1, perhaps due to the noise they add to the training process. However, there's a tradeoff between accuracy and speed.\n",
    "\n",
    "Learning rate has strong influence in the capacity of the model. If learning rate is too large, the gradient can increase the training error and skip local minimas. If learning rate is too small, the training error can get stuck at high values, underfitting the classification task. \n",
    "\n",
    "Lower dropout keep probability may reduce model capacity while reducing the gap between testing and training accuracies. Momentum acts simirlarly to its physical analogue. The gradient will avoid to brutaly change its direction as the momentum increases, like a heavy ball rolling down the hill.\n",
    "\n",
    "With respect to the number of layers to train, it strongly depends on the size of the dataset and its similarity to the original dataset on which the model had been trained. We can define 4 cases:\n",
    "\n",
    "    1. If the current dataset it large and different from the original dataset, consider training from scratch;\n",
    "    2. If the current dataset is large and similar to the original dataset, consider finetuning the model with the pretrained weights;\n",
    "    3. When the current dataset is small and different from the original, it may be better to use some layers in the middle of the network for feature extraction and train a linear classifier over it;\n",
    "    4. Finally, if both datasets are similar but the new one is not that large, it's often useful to use the last layers as feature extractors, maybe finetuning then with a small learning rate.\n",
    "\n",
    "<a id='tensorboard'></a>\n",
    "### 2.2. Tensorboard\n",
    "\n",
    "To evaluate the convergence of the model, tensorboard is a convenient tool:\n",
    "\n",
    "$ tensorboard --logdir path/to/summary/\n",
    "\n",
    "The summaries can be organized by hyperparameter, for example:\n",
    "\n",
    "summary/config1/test/\n",
    "summary/config1/train/\n",
    "summary/config2/test/\n",
    "summary/config2/train/\n",
    "summary/config3/test/\n",
    "summary/config3/train/\n",
    "\n",
    "Then, the tensorboard can be used to compare each configuration."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='training'></a>\n",
    "## 3. Training the model\n",
    "***"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "import numpy as np\n",
    "import json"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The \"config\" below defines the parameters for training. The parameters are:\n",
    "- objs_per_class : minimum of objects per class, for balancing matter.\n",
    "- shuffle_patch_size : number of objects to be shuffled together. For example, it can be useful to set shuffle_path_size = 20 if there're 20 views and if we want to keep them together in the input file.\n",
    "- views_num : number of views per object. This variable is used during the preprocessing to verify if there're missings images.\n",
    "- classes_num : number of classes. It will be used to set the number of neurons in the last layer, and to double-check if the train folder contains the same number of classes\n",
    "- optimizer : three optimizers are implemented\n",
    "    - Momentum\n",
    "    - Adam\n",
    "    - RMSProp\n",
    "- momentum : see Best Practices section\n",
    "- learning_rate : see Best Practices section\n",
    "- decay_factor: for exponential decaying learning rate. If = 1, learning_rate is kept constant.\n",
    "- num_epochs_per_decay: number of epochs per decay.\n",
    "- keep_prob : dropout keep probability\n",
    "- epochs : number of epochs for training. Note that the trainer will always keep the best model regardless of the number of epochs.\n",
    "- batch_size : number of elements per batch\n",
    "- max_to_keep : number of checkpoints we want \n",
    "- vars_to_train : number of variables to train. Use tf.trainable_variables() to see the list of trainable variables. Note that this is specific for each model, so it's better to read the CNN.py code to really understand what's happening underneath.\n",
    "- model : check CNN.py to see which models are available.\n",
    "- weights_path : path to pretrained model. The weights can be found at [https://github.com/tensorflow/models/tree/master/research/slim]\n",
    "- data_dir: path to train and test folders\n",
    "- label_index: where the label of an image in written in its path tree. For example, if the path to the images is e:/Dataset/train/class1/0.png, label_index should be set to -2 or 4.\n",
    "- checkpoint_dir : folder to save the checkpoint files of the model we're training\n",
    "- summary_dir : folder to save the training and testing summary, to be later used with tensorflow\n",
    "- tests_per_epoch : number of times to test the model per training epoch\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    sess.close()\n",
    "except:\n",
    "    pass\n",
    "\n",
    "tf.reset_default_graph()\n",
    "\n",
    "config = {\n",
    "          \"objs_per_class\" : 400,\n",
    "          \"shuffle_patch_size\" : 1,\n",
    "          \"views_num\":20,\n",
    "          \"classes_num\": 10,\n",
    "          \"optimizer\": \"momentum\",\n",
    "          \"momentum\": 0.9,\n",
    "          \"learning_rate\": 0.001,\n",
    "          \"decay_factor\": 1,\n",
    "          \"num_epochs_per_decay\": 1,\n",
    "          \"keep_prob\": 0.65,\n",
    "          \"epochs\": 30,\n",
    "          \"batch_size\": 100,\n",
    "          \"max_to_keep\":1,\n",
    "          \"vars_to_train\":8,\n",
    "          \"model\": \"mobilenet_v1_1.0_224\",\n",
    "          \"weights_path\": \"pretrained/mobilenet_v1_1.0_224.ckpt\",\n",
    "          \"data_dir\": \"C:/tmp/Datasets/ModelNet10/\",\n",
    "          \"label_index\": -3,\n",
    "          \"checkpoint_dir\": \"checkpoints/\",\n",
    "          \"summary_dir\": \"summary/\",\n",
    "          \"tests_per_epoch\": 10,\n",
    "          \"rotationnet_case\": 2\n",
    "          }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Reading text file...\n",
      "Reading text file...\n",
      "Tensor(\"MobilenetV1/MobilenetV1/Conv2d_13_pointwise/Relu6:0\", shape=(?, 7, 7, 1024), dtype=float32)\n",
      "[5, 7, 7, 1024]\n",
      "[CNN] 8 trainable variables\n",
      "[CNN] Training from <tf.Variable 'MobilenetV1/Conv2d_13_depthwise/depthwise_weights:0' shape=(3, 3, 1024, 1) dtype=float32_ref> up to <tf.Variable 'MobilenetV1/Logits/Conv2d_1c_1x1/biases:0' shape=(10,) dtype=float32_ref>\n",
      "Restoring variables up to <tf.Variable 'MobilenetV1/Logits/Conv2d_1c_1x1/biases:0' shape=(10,) dtype=float32_ref>\n",
      "INFO:tensorflow:Restoring parameters from pretrained/mobilenet_v1_1.0_224.ckpt\n",
      "Summary dir created at summary/run8\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "ac769dcf463c4644ae2c2b39def11775",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "HBox(children=(IntProgress(value=0, description='Epoch 0', max=79), HTML(value='')))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Learning rate is 0.0010000003\n",
      "Epoch 0, training loss is 10.990300178527832, recall is 0.22772489795270628, precision is 0.22909078760676133, f1 is 0.2237607425848668, accuracy is 0.0\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "b7272dc9bf3143a89a37b12805afa943",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "HBox(children=(IntProgress(value=0, description='Testing', max=181), HTML(value='')))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Testing loss is 8.083542823791504, recall is 0.833381443298969, precision is 0.664192394421394, f1 is 0.7018446861593983, accuracy is 0.4574585635359116...\n",
      "Saving model...\n",
      "Model saved\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "5ba89962726b4c959fcd961f9a8f7d43",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "HBox(children=(IntProgress(value=0, description='Epoch 0', max=79), HTML(value='')))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Learning rate is 0.0010000003\n",
      "Epoch 0, training loss is 2.41214656829834, recall is 0.39553991264910354, precision is 0.39015648559396, f1 is 0.3906412049853098, accuracy is 0.0\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "583418223f264e99b79ccb6b26f711f2",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "HBox(children=(IntProgress(value=0, description='Testing', max=181), HTML(value='')))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Testing loss is 2.1078505516052246, recall is 0.7140965395988172, precision is 0.5431453774657323, f1 is 0.5795391445791074, accuracy is 0.46298342541436466...\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "dcf1036024034fa3906f76c90678cdff",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "HBox(children=(IntProgress(value=0, description='Epoch 0', max=79), HTML(value='')))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Learning rate is 0.0010000003\n",
      "Epoch 0, training loss is 1.1354504823684692, recall is 0.5544771896951787, precision is 0.5637389413974676, f1 is 0.5491087735861796, accuracy is 0.0\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "668b82b083f64588af41a658c3001f1d",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "HBox(children=(IntProgress(value=0, description='Testing', max=181), HTML(value='')))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Testing loss is 2.4194931983947754, recall is 0.6961395348837209, precision is 0.6360117016723439, f1 is 0.5415529887459581, accuracy is 0.38011049723756907...\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "314909172cf748679312f9955327f45d",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "HBox(children=(IntProgress(value=0, description='Epoch 0', max=79), HTML(value='')))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Learning rate is 0.0010000003\n",
      "Epoch 0, training loss is 1.087835431098938, recall is 0.5593255045120717, precision is 0.5713069577712881, f1 is 0.5599032478281247, accuracy is 0.0\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "5a2794aeb0be4e72ae1fa5d97fadf2bc",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "HBox(children=(IntProgress(value=0, description='Testing', max=181), HTML(value='')))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n"
     ]
    }
   ],
   "source": [
    "from MVCNN.MVCNN import CNN\n",
    "from MVCNN.Dataloader import Data\n",
    "from MVCNN.Trainer import Trainer\n",
    "from MVCNN.Logger import Logger\n",
    "\n",
    "\n",
    "def write_label_txt(encoding):\n",
    "    f = open('class_labels.txt', 'w')\n",
    "    try:\n",
    "        first = True\n",
    "        for key in encoding.keys():\n",
    "            if first:\n",
    "                first = False\n",
    "            else:\n",
    "                f.write('\\n')\n",
    "            f.write(str(encoding[key]))\n",
    "    finally:\n",
    "        f.close()\n",
    "    \n",
    "configProto = tf.ConfigProto(allow_soft_placement = True)\n",
    "#create tensorflow session\n",
    "sess = tf.Session(config = configProto)\n",
    "#data class\n",
    "data = Data('train_rotNet.txt','test_rotNet.txt',config,sess, verbose = False)\n",
    "#write class_labels.txt file\n",
    "write_label_txt(data.label_encoder.i_encoding)\n",
    "\n",
    "#defining model\n",
    "model = CNN(data,config)\n",
    "#loading initial weights\n",
    "model.load_initial_weights(sess)\n",
    "#initializing variables that aren't in the pretrained checkpoint\n",
    "model.initialize_uninitialized(sess)\n",
    "#logger\n",
    "logger = Logger(sess,config)\n",
    "#trainer\n",
    "trainer = Trainer(sess, model, data, config, logger)\n",
    "#load model if exists\n",
    "#model.load_trainable(sess)\n",
    "#train\n",
    "try:\n",
    "    trainer.train()\n",
    "except KeyboardInterrupt:\n",
    "    pass\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='freezing'></a>\n",
    "## 4. Freezing the graph\n",
    "***\n",
    "\n",
    "In this section, we use our last saved model to create a .pb file. This file type can be used to serve Android Apps and to convert to .mlmodel for iOS apps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sess.close()\n",
    "tf.reset_default_graph()\n",
    "\n",
    "#create new tensorflow session\n",
    "sess = tf.Session(config = configProto)\n",
    "#where to save the frozen graph\n",
    "output_graph = 'frozengraphs/mobilenet.pb'\n",
    "#the name of the output layer (usually the softmax layer)\n",
    "output_node_names = ['train/loss/Softmax']\n",
    "\n",
    "#new_saver = tf.train.import_meta_graph(tf.train.latest_checkpoint(config['checkpoint_dir']) + '.meta')\n",
    "#new_saver.restore(sess,tf.train.latest_checkpoint(config['checkpoint_dir']))\n",
    "\n",
    "model = CNN(data,config,deploy = True)\n",
    "model.load(sess)\n",
    "\n",
    "\n",
    "# We use a built-in TF helper to export variables to constants\n",
    "output_graph_def = tf.graph_util.convert_variables_to_constants(\n",
    "    sess, # The session is used to retrieve the weights\n",
    "    tf.get_default_graph().as_graph_def(), # The graph_def is used to retrieve the nodes \n",
    "    output_node_names # The output node names are used to select the usefull nodes\n",
    ") \n",
    "\n",
    "# Finally we serialize and dump the output graph to the filesystem\n",
    "with tf.gfile.GFile(output_graph, \"wb\") as f:\n",
    "    f.write(output_graph_def.SerializeToString())\n",
    "print(\"%d ops in the final graph.\" % len(output_graph_def.node))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='converting'></a>\n",
    "## 5. Converting to CoreML\n",
    "***\n",
    "\n",
    "After installing coremltools and tfcoreml with pip, run the code below to create the CoreML model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#path to save the .mlmodel\n",
    "output_mlmodel = 'coremlmodels/mobilenet.mlmodel'\n",
    "import tfcoreml as tf_converter\n",
    "\n",
    "tf_converter.convert(tf_model_path = output_graph,\n",
    "                     mlmodel_path = output_mlmodel,\n",
    "                     output_feature_names = ['train/loss/Softmax:0'],\n",
    "                     image_input_names = 'image:0',\n",
    "                     class_labels = 'class_labels.txt',\n",
    "                     image_scale = 1.0/255.0\n",
    "                     )\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
