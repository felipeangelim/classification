# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 10:37:46 2018

@author: FVA3
"""

# -*- coding: utf-8 -*-
"""
Created on Thu May 24 16:27:25 2018
"""
import sklearn
from sklearn.preprocessing import LabelEncoder
import json
import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from imblearn.over_sampling import SMOTE
from tqdm import tqdm
from random import sample
from pandas_ml import ConfusionMatrix
import pandas_ml as pdml
import matplotlib.pyplot as plt

def get_signature(signature):
    if type(signature) == list:
        return signature
    else:
        return list(map(float,signature.split(' ')))
    
def get_categories(Y,level):
    '''
    
    :param Y: list of strings (containing the labels)
    returns the list after applying 'name at level' to each label
    '''
    for i in range(len(Y)):
        Y[i] = name_at_level(Y[i],level)
    return Y

def name_at_level(string,level):
    '''
    receives a string with the entire label for a certain object
    and cuts the right half
    :param string: the label as a string
    :param level: desired level
    '''
    index = -len(string.split('/'))+level+3
    index = min(len(string.split('/')) + index, len(string.split('/'))+1)
    out = '/'.join(string.split('/')[:index])
    if out[-1] != '/':
        out += '/'
    '''
    string = string.split('mechanical-systems-and-components-of-general-use/')[-1]
    string = string.split('/')
    string = string[min(level-1,len(string)-1)]
    '''
    return get_radical(out)

def get_radical(name):
    return name.replace('/ics-traceparts-classification/mechanical-systems-and-components-of-general-use/','')

def json_to_npy(path, level = 3):
    f = open(path,'r')
    try:
        parsed = json.load(f)
    finally:
        f.close()
    features = []
    labels = []
    for obj in tqdm(parsed):
        features.append(get_signature(obj['shape_3D_signature']))
        labels.append(obj['label'])
    
    labels = get_categories(labels,level)
    return np.array(features),np.array(labels)

def rank(labels,probs):
    ranks = []
    for i in range(labels.shape[0]):
        label = labels[i]
        prob = probs[i]
        prob_of_tp = prob[label]
        if prob_of_tp == 0:
            ranks.append(100)
        else:
            ranks.append(np.min(np.where(np.flip(np.sort(prob), axis = 0) == prob_of_tp)))
    return np.array(ranks)
    
path_train = 'e:/Datasets/new_train_embeddings.json'
#path_train = 'e:/Datasets/trainList_416_ClusterCentroid_400max.json'
path_test = 'e:/Datasets/new_test_embeddings.json'

#parameters
depthForClassif = 3
k_neigh = 400
over_sample = True

#loading data
features_train,labels_train = json_to_npy(path_train,depthForClassif)
features_test,labels_test = json_to_npy(path_test,depthForClassif)

#encoding string labels to numbers
encoder = LabelEncoder()
encoder.fit(labels_train)
labels_train = encoder.transform(labels_train)
labels_test = encoder.transform(labels_test)

if over_sample:
    smote = SMOTE(ratio = 'all', k = 1)
    smote.fit_sample(features_train,labels_train)

#creating kNN classifier
neigh = KNeighborsClassifier(n_neighbors=k_neigh, n_jobs = -1)
#fitting
neigh.fit(features_train,labels_train)
probs = neigh.predict_proba(features_test)

ranking = rank(labels_test,probs)

for i in [1,3,5,10]:
    print("Top {}: ".format(i),np.sum(ranking < i)/ranking.shape[0]) 