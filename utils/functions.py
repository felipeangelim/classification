import numpy as np

def parse_data(array):
    signatures = []
    labels = []
    for item in array:
        signatures.append(item['shape_3D_signature'])
        labels.append(item['label'])
    return np.array(signatures), np.array(labels)

def precision_recall_f1(labels,predictions):
        uniques = np.unique(labels)
        #number of classes
        n = uniques.shape[0]
        precision = []
        recall = []
        f1 = []
        for k in range(n):
            cur_label = uniques[k]
            tp = ((labels == cur_label) & (predictions == cur_label)).sum()
            fp = ((labels != cur_label) & (predictions == cur_label)).sum()
            fn = ((labels == cur_label) & (predictions != cur_label)).sum()
            if tp + fp != 0:
                recall.append(tp/(tp + fn))
                if tp == 0:
                    precision.append(0)
                    f1.append(0)
                else:
                    precision.append(tp/(tp + fp))
                    f1.append(2*precision[-1]*recall[-1]/(precision[-1] + recall[-1]))
        precision = np.array(precision)
        recall = np.array(recall)
        f1 = np.array(f1)
        return np.mean(precision), np.mean(recall), np.mean(f1)
    
def parse_data_filter(array, labels_to_keep):
    signatures = []
    labels = []
    
    for i in range(len(labels_to_keep)):
        labels_to_keep[i] = labels_to_keep[i].replace(' ','').split('/')[-1]
    check = { key: 0 for key in labels_to_keep}
    for item in array:
        if item['label'].split('/')[-2] in labels_to_keep:
            check[item['label'].split('/')[-2]] += 1
            signatures.append(item['shape_3D_signature'])
            labels.append(item['label'])
    for key in check.keys():
        if check[key] == 0:
            print('WARNING', key,' has 0')
    assert(len(labels) > 0)
    return np.array(signatures), np.array(labels)

def get_classes_under_threshold(labels, threshold):
    from collections import Counter 
    counter = Counter(labels)
    minority = []
    for key in counter.keys():
        if counter[key] < threshold:
            minority.append(key)
    return minority

class Normalizer:
    
    def __init__(self,x_train):
        self.means = np.mean(x_train, axis = 0)
        self.std = np.std(x_train, axis = 0)
        
    def normalize(self,input_array):
        rows = input_array.shape[0]
        means = np.tile(np.expand_dims(self.means, axis = -1),[1,rows])
        means = np.transpose(means)
        
        std = np.tile(np.expand_dims(self.std, axis = -1),[1,rows])
        std = np.transpose(std)
    
        return (input_array - means)/std
